# NewSolFrontend (q-front)

This project compat width NodeJS v16 max

with [nvm](https://github.com/nvm-sh/nvm):
```bash
nmv use 16
```

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
yarn dev
```


### Build the app for production
```bash
yarn build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

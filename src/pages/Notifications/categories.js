export default {
  shipping: { color: "brown", icon: "local_shipping" },
  payments: { color: "deep-orange", icon: "price_check" },
  account: { color: "purple", icon: "person" },
  disputes: { color: "indigo", icon: "announcement" },
  sales: { color: "green", icon: "point_of_sale" },
  buyings: { color: "teal", icon: "shopping_bag" },
  system: { color: "accent", icon: "settings_suggest" },
  announcements: { color: "orange", icon: "campaign" },
};

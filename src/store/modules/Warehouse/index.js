const ACTIONS = {
  GET_PRODUCTS: "GET_PRODUCTS",
  GET_CATEGORIES: "GET_CATEGORIES",
  CREATE_CATEGORY: "CREATE_CATEGORY",
  SAVE_CATEGORY: "SAVE_CATEGORY",
};

const MUTATIONS = {
  SET_PRODUCTS: "SET_PRODUCTS",
  SET_CATEGORIES: "SET_CATEGORIES",
  SET_TOTAL: "SET_TOTAL",
};

export default {
  namespaced: true,
  state() {
    return {
      products: null,
      categories: null,
      total: null,
    };
  },
  actions: {
    [ACTIONS.GET_PRODUCTS]: async ({ commit }, { warehouse = false, ...params }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { products, total },
          } = await $api({
            url: `/warehouse/get_all_products${warehouse ? "/warehouse" : ""}`,
            params,
          });

          done({ products, total });

          // commit(MUTATIONS.SET_PRODUCTS, products);
          // commit(MUTATIONS.SET_TOTAL, total);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_CATEGORIES]: async ({ commit }) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: {
              category_tree: { folders },
            },
          } = await $api({
            url: `/warehouse/get_tree`,
          });

          done(folders);

          commit(MUTATIONS.SET_CATEGORIES, folders);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.CREATE_CATEGORY]: async ({ commit, dispatch }, category) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { category: created },
          } = await $api.post(`/warehouse/categories`, category);

          done(created);

          // TODO:
          // dispatch(ACTIONS.GET_PRODUCTS)
          // and remove in external initiators
        } catch (e) {
          console.log(e);

          fail(e);
        }
      });
    },
    [ACTIONS.SAVE_CATEGORY]: async ({ commit }, category) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { category: updated },
          } = await $api({
            url: `/warehouse/categories`,
            method: "PUT",
            data: category,
          });

          done(updated);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_PRODUCTS](state, products) {
      state.products = products;
    },
    [MUTATIONS.SET_CATEGORIES](state, categories) {
      state.categories = categories;
    },
    [MUTATIONS.SET_TOTAL](state, total) {
      state.total = total;
    },
  },
  getters: {
    categories: (state) => state.categories,
  },
};

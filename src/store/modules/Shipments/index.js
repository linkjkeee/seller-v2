export default {
  namespaced: true,
  state: () => ({
    checked: [],
    list: [],
    types: {
      "direct": {
        label: "Отправка из Украины",
        tlabel: "types.direct",
        help: null,
        disabled: false,
        show: true,
        description:
          'Заявка на отправку посылки из Украины. Заполните адрес получателя и выберите тип отправки "Экспресс" или "Консолидация" при создании посылки.',
      },
      "svh": {
        label: "Отправка на склад в США",
        tlabel: "types.svh",
        help: "https://seller-online.com/help/zayavka-na-otpravku-posylki-na-nash-sklad-v-ssha",
        disabled: false,
        show: true,
        description:
          "Заявка на отправку посылок для хранения на склад в США. В дальнейшем после продажи товара отправка происходит непосредственно со склада, что позволяет существенно сократить сроки доставки.",
      },
      "usa_domestic": {
        label: "Отправка со склада в США",
        tlabel: "types.usa_domestic",
        help: "https://seller-online.com/help/oformlenie-zayavki-na-otpravku-so-sklada-v-ssha",
        disabled: false,
        show: true,
        description: "Заявка на отправку товара, находящегося на хранении на складе в США.",
      },
      "consolidation_usa": {
        label: "Global forward",
        tlabel: "types.global_forward",
        help: null,
        disabled: false,
        show: true,
        description: null,
      },
      "consolidated_from_sender": {
        label: "Консолидированная от продавца",
        tlabel: "types.cosolidation_from_sender",
        help: null,
        disabled: false,
        show: true,
        description: null,
      },
      "return_label": {
        label: "Возвратная отправка",
        tlabel: "types.return_label",
        help: null,
        disabled: false,
        show: true,
        description: null,
      },
    },
  }),
  actions: {
    GET_SHIPMENTS: (ctx, params) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api({
            type: "get",
            url: "/shipping/shipment",
            params,
          });

          ctx.commit("SET_SHIPMENTS", data.shipments);
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    SHIPMENT_REMOVE: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          const request = await $api.delete(`/shipping/shipment`, {
            data: { shipment_id },
          });
          done(request);
          ctx.commit("REMOVE_FORM_VIEW", shipment_id);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_PARCEL_STICKERS: (ctx, { shipment_ids, print_format = "A4" }) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api({
            method: "POST",
            url: `/v2/shipping/shipments/get-stickers`,
            data: { shipment_ids, print_format },
            responseType: "arraybuffer",
          });
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_PARCEL_PRODUCTS: (ctx, parcel_id) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { products },
          } = await $api.get(`/shipping/parcel_products/${parcel_id}`);
          done(products);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_CONSOLIDATION_DOCUMENTS: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = $api.post(`/`, { shipment_id });
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_SHIPMENTS_BY_ACTION: (ctx, action) => {
      return new Promise(async (done, fail) => {
        if (!action) fail("No action specified");

        try {
          let { data } = api.get(`/shipping/shipment?action=${action}`);
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_SHIPPING_DOCS_COST: (ctx, { shipper, shipment_id, dimensions, total_weight }) => {
      return new Promise(async (done, fail) => {
        let params = { shipper, shipment_id, dimensions, total_weight };
        try {
          const { data } = await $api.post(`/shipping/shipment/docs/price/`, params);
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    CREATE_CONSOLIDATION_LABEL: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.post(`/shipping/label/consolidation`, { shipment_id });
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_CONSOLIDATION_COST: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.post(`/shipping/rates/consolidation`, {
            shipment_id,
          });
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_SHIPMENT_LABEL: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.get(`/shipping/label/by_shipment/${shipment_id}`, {
            responseType: "arraybuffer",
          });
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_SHIPMENT_PROFORMA: (ctx, shipment_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.get(`/shipping/shipment/proforma/${shipment_id}`);
          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_WAREHOUSE_STICKERS: (ctx, { shipment_id, print_format }) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api({
            url: "/shipping/print-warehouse-product-stickers",
            method: "POST",
            responseType: "arraybuffer",
            data: {
              shipment_id,
              print_format,
            },
          });

          done(data);
        } catch (e) {
          fail(e);
        }
      });
    },
    CREATE_DHL_DOCUMENTS: (ctx, shipment_id = null) => {
      // ? GET DHL CREATED /shipping/shipment?action=create_label_consolidation
      // ? OR SOME SHIPMENT
      new Promise(async (done, fail) => {
        try {
        } catch (e) {
          console.log(e);
        } finally {
        }
      });
    },
  },
  mutations: {
    UPADTE_CHECKED: (state, { shipment, checked }) => {
      if (checked) state.checked.push(shipment);
      else {
        let idx = state.checked.findIndex((s) => s.id === shipment.id);
        state.checked.splice(idx, 1);
      }
    },
    CLEAR_CHECKED: (state) => {
      state.checked = [];
    },
    SET_SHIPMENTS: (state, shipments) => {
      state.list = shipments;
    },
    REMOVE_FORM_VIEW: (state, shipment_id) => {
      let idx = state.list.findIndex((s) => s.id === shipment_id);
      state.list.splice(idx, 1);
    },
  },
  getters: {
    checked_shipments: (state) => state.checked,
    shipments_list: (state) => state.list,
    types: (state, ctx) => {
      return state.types;
    },
  },
};

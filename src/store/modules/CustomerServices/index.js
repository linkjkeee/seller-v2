const ACTIONS = {
  GET_ALL_SERVICES: "GET_ALL_SERVICES",
  GET_PERIODS: "GET_PERIODS",
  GET_DEPARTMENTS: "GET_DEPARTMENTS",
  GET_STATUSES: "GET_STATUSES",
  GET_SERVICE_ORDER: "GET_SERVICE_ORDER",
  GET_ORDERED_SERVICES: "GET_ORDERED_SERVICES",
  PROCESS_SERVICE_ORDER: "PROCESS_SERVICE_ORDER",
  GET_SERVICE: "GET_SERVICE",
};
const MUTATIONS = {
  SET_ALL_SERVICES: "SET_SERVICES",
  SET_PERIODS: "SET_PERIODS",
  SET_DEPARTMENTS: "SET_DEPARTMENTS",
  SET_STATUSES: "SET_STATUSES",
};

export default {
  namespaced: true,
  state() {
    return {
      services: null,
      periods: null,
      statuses: null,
      departments: null,
    };
  },
  actions: {
    [ACTIONS.GET_ALL_SERVICES]: async (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { services, preferred_lang },
          } = await $api(`/service-for-customers/`);

          ctx.commit(MUTATIONS.SET_ALL_SERVICES, services);

          done({ services, lang: preferred_lang });
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_PERIODS]: async (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { periods },
          } = await $api.get(`/service-for-customers/terms`);

          ctx.commit(MUTATIONS.SET_PERIODS);

          done(periods);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_STATUSES]: async (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { preferred_lang, statuses },
          } = await $api(`/service-for-customers/statuses`);

          done(statuses);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_DEPARTMENTS]: async (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { departments },
          } = await $api.get(`/service-for-customers/departments`);

          done(departments);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_ORDERED_SERVICES]: async (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { orders, preferred_lang = null },
          } = await $api(`/service-for-customers/order`);

          done({ orders, lang: preferred_lang || "ru" });
        } catch (e) {
          console.log(e);

          fail(e);
        }
      });
    },
    [ACTIONS.GET_SERVICE_ORDER]: async (ctx, order_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api(`/service-for-customers/order/${order_id}`);

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.PROCESS_SERVICE_ORDER]: (ctx, { action, order_id }) => {
      return new Promise(async (done, fail) => {
        try {
          await $api.put(`/service-for-customers/order/${action}`, {
            order_id,
          });

          done(true);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_SERVICE]: (ctx, service_id) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api(`/service-for-customers/${service_id}`);

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_ALL_SERVICES]: (state, services) => {
      state.services = services;
    },
    [MUTATIONS.SET_PERIODS]: (state, periods) => {
      state.periods = periods;
    },
  },
  getters: {},
};

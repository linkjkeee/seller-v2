const LABEL_FORMATS = [
  {
    label: "A5",
    value: "SIZE_A5",
  },
  {
    label: "A6",
    value: "SIZE_A6",
  },
  {
    label: "10x10",
    value: "SIZE_10X10",
  },
];

const ACTIONS = {
  GET_SHIPMENTS: "GET_SHIPMENTS",
  GET_LABEL: "GET_LABEL",
  DELETE_SHIPMENT: "DELETE_SHIPMENT",
};

export default {
  namespaced: true,
  //   state: () => ({
  //     shipments: null,
  //   }),
  actions: {
    [ACTIONS.GET_SHIPMENTS]: (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { shipments },
          } = await $api.get(`/ukrposhta/shipments`);
          done(shipments);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_LABEL]: (ctx, { uuid, format }) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api({
            type: "get",
            url: `/ukrposhta/docs/${uuid}?doc_type=cn22&doc_size=${format}`,
            responseType: "arraybuffer",
          });
          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.DELETE_SHIPMENT]: (ctx, uuid) => {
      new Promise(async (done, fail) => {
        try {
          await $api.delete(`/ukrposhta/shipments/${uuid}`);
          done(uuid);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {},
  getters: {
    formats: () => LABEL_FORMATS,
  },
};

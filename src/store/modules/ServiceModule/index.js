const ACTIONS = {
  UPLOAD_FILE: "UPLOAD_FILE",
};

export default {
  namespaced: true,
  state: () => ({}),
  actions: {
    [ACTIONS.UPLOAD_FILE]: (ctx, file) => {
      return new Promise(async (done, fail) => {
        try {
          let fd = new FormData();
          fd.append("file", file);

          const {
            data: { filename },
          } = await $api.post(`maintenance/upload`, fd);
          done(filename);
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.DOWNLOAD_FILE]: (ctx, filename) => {
      return new Promise((ok, fail) => {
        try {
        } catch (e) {
          console.log(e);
        } finally {
        }
      });
    },
  },
  mutations: {},
  getters: {},
};

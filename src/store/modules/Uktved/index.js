const ACTIONS = {
  LOAD_CHILDRENS: "LOAD_CHILDRENS",
};

export default {
  namespaced: true,
  state() {
    return {};
  },
  actions: {
    [ACTIONS.LOAD_CHILDRENS]: (ctx, { params, item_id }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { children },
          } = await $api(`v2/products/uktved/${item_id}`, {
            params,
          });

          done(children);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
};

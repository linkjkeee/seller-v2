const ACTIONS = {
  GET_TYPES: "GET_TYPES",
  GET_DATA: "GET_DATA",
  SAVE_ADDRESS: "SAVE_ADDRESS",
  GET_ADDRESSES: "GET_ADDRESSES",
  GET_ADDRESS: "GET_ADDRESS",
  UPDATE_ADDRESS: "UPDATE_ADDRESS",
};

const MUTATIONS = {
  SET_TYPES: "SET_TYPES",
  SET_ADDRESSES: "SET_ADDRESSES",
};

export default {
  namespaced: true,
  state: () => ({
    types: null,
    book: null,
  }),
  actions: {
    [ACTIONS.GET_TYPES]: (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { modules },
          } = await $api.get(`/user/book/modules`);

          ctx.commit(MUTATIONS.SET_TYPES, modules);
          done(modules);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_DATA]: (ctx, { type, ...params }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { data },
          } = await $api({
            url: `/user/book/fields/${type}/data`,
            params,
          });

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.SAVE_ADDRESS]: (ctx, { variation_type, fields, book_type }) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api.post(`/user/book/`, {
            ...fields,
            book_type,
            variation_type,
          });

          done();
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_ADDRESSES]: (ctx, params = {}) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { address_book },
          } = await $api({
            url: `/user/book`,
            params,
          });

          ctx.commit(MUTATIONS.SET_ADDRESSES, address_book);
          done(address_book);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_ADDRESS]: (ctx, id) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { address_book_item },
          } = await $api.get(`/user/book/${id}`);

          done(address_book_item);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.UPDATE_ADDRESS]: (ctx, { address_id, fields, variation_type, book_type }) => {
      return new Promise(async (done, fail) => {
        try {
          const { data } = await $api.put(`/user/book/${address_id}`, {
            ...fields,
            variation_type,
            book_type,
          });

          done();
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_TYPES]: (ctx, types) => {
      ctx.types = types;
    },
    [MUTATIONS.SET_ADDRESSES]: (ctx, addresses) => {
      ctx.book = addresses;
    },
  },
  getters: {
    types(state) {
      return state.types;
    },
    book(state) {
      return state.book;
    },
  },
};

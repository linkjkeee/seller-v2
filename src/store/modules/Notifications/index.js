const ACTIONS = {
  GET_NOTIFICATIONS: "GET_NOTIFICATIONS",
  GET_COUNT: "GET_COUNT",
  GET_ONE_NOTIFICATION: "GET_ONE_NOTIFICATION",
  APPEND_NOTIFICATIONS: "APPEND_NOTIFICATIONS",
  CHANGE_EVENT_STATE: "CHANGE_EVENT_STATE",
};

const MUTATIONS = {
  SET_NOTIFICATIONS: "SET_NOTIFICATIONS",
  SET_COUNT: "SET_COUNT",
  SET_NEXT_PAGE: "SET_NEXT_PAGE",
  SET_TOTAL_PAGES: "SET_TOTAL_PAGES",
  SET_LOADING: "SET_LOADING",
  APPEND_EVENTS: "APPEND_EVENTS",
};

export default {
  namespaced: true,
  state: () => ({
    notifications: null,
    counter: null,
    next_page: null,
    total: null,
    loading: false,
  }),
  actions: {
    [ACTIONS.GET_COUNT]: (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { total },
          } = await $api.get(`/notifications`, {
            params: {
              only_number: true,
            },
          });

          ctx.commit(MUTATIONS.SET_COUNT, total);
          done(total);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_NOTIFICATIONS]: (ctx, params = {}) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { next_page, notifications, on_page, page, total },
          } = await $api({
            url: `/notifications`,
            params,
          });

          // ctx.commit(MUTATIONS.SET_NEXT_PAGE, next_page);
          ctx.commit(MUTATIONS.SET_TOTAL_PAGES, total);
          if (params.set) {
            ctx.commit(MUTATIONS.SET_NOTIFICATIONS, notifications);
          }

          done({ notifications, next_page });
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.GET_ONE_NOTIFICATION]: (ctx, { id, is_pinned = false }) => {
      return new Promise(async (done, fail) => {
        try {
          let {
            data: { notification },
          } = await $api.get(`/notifications/${id}`);

          is_pinned && (notification.is_pinned = true);

          done(notification);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.CHANGE_EVENT_STATE]: (ctx, { id, action }) => {
      return new Promise(async (done, fail) => {
        try {
          await $api.post(`/notifications/${id}`, action);
          done(true);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_NEXT_PAGE]: (state, data) => {
      state.next_page = data;
    },
    [MUTATIONS.SET_TOTAL_PAGES]: (state, data) => {
      state.total = data;
    },
    [MUTATIONS.SET_NOTIFICATIONS]: (state, data) => {
      state.notifications = data;
    },
    [MUTATIONS.SET_COUNT]: (state, count) => {
      state.counter = count;
    },
    [MUTATIONS.SET_LOADING]: (state, loading) => {
      state.loading = loading;
    },
    [MUTATIONS.APPEND_EVENTS]: (state, new_events) => {
      if (!new_events || !new_events.length) return;
      state.notifications.push(...new_events);
    },
  },
  getters: {
    notifications: (state) => state.notifications,
    count: (state) => state.counter,
    loading: (state) => state.loading,
  },
};

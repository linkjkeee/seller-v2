const ACTIONS = {
  GET_REG_COUNTRIES: "GET_REG_COUNTRIES",
  GET_COUNTRIES: "GET_COUNTRIES",
  GET_STATES: "GET_STATES",
  GET_SHIPMENT_STATUSES: "GET_SHIPMENT_STATUSES",
};

const MUTATIONS = {
  SET_COUNTRIES: "SET_COUNTRIES",
  SET_STATES: "SET_STATES",
};

export default {
  namespaced: true,
  state: () => ({
    countries: [],
    states: [],
  }),
  actions: {
    [ACTIONS.GET_REG_COUNTRIES]: async ({ commit }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { countries },
          } = await $api.get(`info/countries/`);

          done(countries);
        } catch (e) {
          fail(e);
        } finally {
        }
      });
    },
    [ACTIONS.GET_COUNTRIES]: ({ commit, state }) => {
      return new Promise(async (done, fail) => {
        if (state.countries && state.countries.length) done(state.countries);

        try {
          const {
            data: { countries },
          } = await $api.get("/shipping/get_countries");

          commit("SET_COUNTRIES", countries);

          done(countries);
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.GET_STATES]: async ({ commit }, country) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { states },
          } = await $api.get(`/shipping/get_states/${country}`);
          commit("SET_STATES", states);
          done(states);
        } catch (e) {
          fail(e);
        }
      });
    },
    GET_SHIPMENT_STATUSES: async ({ commit }) => {
      return new Promise(async (done, fail) => {
        try {
          const {
            data: { statuses },
          } = await $api.get(`/shipping/get_shipment_statuses`);
          done(statuses);
        } catch (e) {
          fail(e);
        } finally {
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_COUNTRIES]: (state, countries) => {
      let normalized = Object.entries(countries).map(([code, title]) => ({
        label: title,
        value: code,
      }));

      state.countries = normalized;
    },
    [MUTATIONS.SET_STATES]: (state, states) => {
      let normalize = Object.entries(states).map(([code, title]) => ({
        label: title,
        value: code,
      }));
      state.states = normalize;
    },
  },
  getters: {
    countries: (state) => state.countries,
    states: (state) => state.states,
  },
};

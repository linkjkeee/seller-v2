const ACTIONS = {
  GET_BALANCE: "GET_BALANCE",
  GET_CURRENCY_RATES: "GET_CURRENCY_RATES",
};
const MUTATIONS = {
  SET_BALANCE: "SET_BALANCE",
  SET_CURRENCY_RATES: "SET_CURRENCY_RATES",
};

export default {
  namespaced: true,
  state: () => ({
    balance: null,
    currency_rates: [],
  }),
  actions: {
    [ACTIONS.GET_BALANCE]: (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          const { data: balance } = await $api.get(`/user/balance`);
          ctx.commit("SET_BALANCE", balance.balance);
          done(balance);
        } catch (e) {
          fail(e);
        }
      });
    },
    [ACTIONS.GET_CURRENCY_RATES]: (ctx) => {
      return new Promise(async (done, fail) => {
        try {
          const { data: rates } = await $api.get(`/info/rates`);

          ctx.commit("SET_CURRENCY_RATES", rates.rates);
          done(rates);
        } catch (e) {
          console.debug(e);
          fail(e);
        }
      });
    },
  },
  mutations: {
    [MUTATIONS.SET_BALANCE]: (state, balance) => {
      state.balance = balance;
    },
    [MUTATIONS.SET_CURRENCY_RATES]: (state, rates) => {
      state.currency_rates = rates;
    },
  },
  getters: {
    balance: (state) => state.balance,
    currency_rates: (state) => state.currency_rates,
  },
};

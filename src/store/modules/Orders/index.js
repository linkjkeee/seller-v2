const ACTIONS = {
  ADD_TRACKS: "ADD_TRACKS",
  CANCEL_ORDER: "CANCEL_ORDER",
};

export default {
  namespaced: true,
  state: () => ({}),
  actions: {
    [ACTIONS.ADD_TRACKS]: (ctx, { track_numbers, order_id }) => {
      return $api.put(`/orders/${order_id}/set_track`, {
        track_numbers,
      });
    },
    [ACTIONS.CANCEL_ORDER]: (ctx, order_id) => {
      return $api.put(`/orders/${order_id}/cancel`);
    },
  },
};

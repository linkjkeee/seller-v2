const ACTIONS = {
  GET_PRODUCTS: "GET_PRODUCTS",
};

export default {
  namespaced: true,
  state() {
    return {};
  },
  actions: {
    [ACTIONS.GET_PRODUCTS]: (ctx, params = {}) => {
      let { warehouse = false } = params;

      return new Promise(async (done, fail) => {
        try {
          let {
            data: { products },
          } = await $api.get(`/warehouse/get_all_products${warehouse ? "/warehouse/" : ""}`);

          done(products);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutatuions: {},
  getters: {},
};

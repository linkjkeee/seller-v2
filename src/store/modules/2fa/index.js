const ACTIONS = {
  GET_STATUS: "GET_STATUS",
  GET_CREDENTIALS: "GET_CREDENTIALS",
  SAVE_CREDENTIALS: "SAVE_CREDENTIALS",
  TURN_OFF: "TURN_OFF",
};
const MUTATIONS = {};

export default {
  namespaced: true,
  state() {
    return {};
  },
  actions: {
    [ACTIONS.GET_STATUS]: (
      {
        commit,
        // dispatch,
        // state,
        // rootState
      },
      payload
    ) => {
      return new Promise(async (done, fail) => {
        try {
          // ? /auth/ext/setup GET

          let { data } = await $api(`/auth/ext/setup`);

          done(data);
        } catch (e) {
          console.log(e);

          fail(e);
        }
      });
    },
    [ACTIONS.GET_CREDENTIALS]: (ctx, { add = false, ...params }) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.post(add ? `/auth/ext/add` : `/auth/ext/setup`, params);

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.SAVE_CREDENTIALS]: (ctx, params) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.put(`/auth/ext/setup`, params);
          done();
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
    [ACTIONS.TURN_OFF]: (ctx, params) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.delete(`/auth/ext/setup`, { data: params });

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
};

const ACTIONS = {
  FINISH_INTEGRATION: "FINISH_INTEGRATION",
};

export default {
  namespaced: true,
  state: () => ({}),
  actions: {
    [ACTIONS.FINISH_INTEGRATION]: (ctx, { type, params }) => {
      return new Promise(async (done, fail) => {
        try {
          let { data } = await $api.post(`/${type}/finish-integration`, {
            ...params,
          });

          done(data);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
};

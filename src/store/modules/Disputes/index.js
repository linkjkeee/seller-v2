const ACTIONS = {
  GET_DISPUTE: "GET_DISPUTE",
};

export default {
  namespaced: true,
  state: () => ({
    reasons: {
      MERCHANDISE_OR_SERVICE_NOT_RECEIVED: {
        text: "ТОВАР ИЛИ УСЛУГА НЕ ПОЛУЧЕНЫ",
        level: "danger",
        icon: "report_problem",
      },
      MERCHANDISE_OR_SERVICE_NOT_AS_DESCRIBED: {
        text: "ТОВАР ИЛИ УСЛУГА НЕ СООТВЕТСТВУЕТ ОПИСАНИЮ",
        level: "warning",
        icon: "production_quantity_limits",
      },
      UNAUTHORISED: { text: "ПОКУПАТЕЛЬ НЕ РАЗРЕШИЛ ПОКУПКУ ТОВАРА ИЛИ УСЛУГИ", level: "warning" },
      CREDIT_NOT_PROCESSED: { text: "CREDIT_NOT_PROCESSED", level: "negative" },
      DUPLICATE_TRANSACTION: { text: "DUPLICATE_TRANSACTION", level: "warning" },
      INCORRECT_AMOUNT: { text: "НЕВЕРНАЯ СУММА СПИСАНИЯ", level: "danger" },
      PAYMENT_BY_OTHER_MEANS: {
        text: "КЛИЕНТ ОПЛАТИЛ ТРАНЗАКЦИЮ ДРУГИМИ СПОСОБАМИ",
        level: "info",
      },
      CANCELED_RECURRING_BILLING: { text: "ОТМЕНА ПОВТОРНОГО СЧЕТА", level: "info" },
      PROBLEM_WITH_REMITTANCE: { text: "ПРОБЛЕМА С ДЕНЕЖНЫМ ПЕРЕВОДОМ", level: "danger" },
      CREDIT_NOT_PROCESSED: { text: "ПЛАТЕЖ НЕ ОБРАБОТАН", level: "info" },
      OTHER: { text: "ДРУГОЕ", level: "info" },
    },
    statuses: {
      OPEN: { text: "ОТКРЫТ", level: "" },
      WAITING_FOR_BUYER_RESPONSE: { text: "ОЖИДАНИЕ ОТВЕТА ПОКУПАТЕЛЯ", level: "" },
      WAITING_FOR_SELLER_RESPONSE: { text: "ОЖИДАНИЕ ОТВЕТА ПРОДАВЦА", level: "" },
      UNDER_REVIEW: { text: "В РАССМОТРЕНИИ", level: "" },
      RESOLVED: { text: "РАЗРЕШЁН", level: "" },
      OTHER: { text: "ДРУГОЙ", level: "" },
    },
    client_actions: {
      "send-message": "Отправить сообщение клиенту",
      "accept-claim": "Компенсировать стоимость",
      "provide-evidence": "Предоставить доказательства",
    },
  }),
  actions: {
    [ACTIONS.GET_DISPUTE]: (ctx, dispute_id) => {
      return new Promise(async (done, fail) => {
        try {
          const { data: dispute } = await $api(`/disputes/${dispute_id}`);

          done(dispute);
        } catch (e) {
          console.log(e);
          fail(e);
        }
      });
    },
  },
  mutations: {},
  getters: {
    reasons(state) {
      return state.reasons;
    },
    statuses(state) {
      return state.statuses;
    },
    actions(state) {
      return state.client_actions;
    },
  },
};

const MUTATIONS = {
  CART_ADD: "CART_ADD",
  CART_REMOVE: "CART_REMOVE",
  CART_CLEAR: "CART_CLEAR",
  CART_UPDATE: "CART_UPDATE",
};

const ACTIONS = {
  CART_ADD: "CART_ADD",
  CART_REMOVE: "CART_REMOVE",
  CHANGE_ITEM_COUNT: "CHANGE_ITEM_COUNT",
};

const CART_STORAGE_KEY = "sol-cart-content";

const CartGetItems = () => {
  let res = localStorage.getItem(CART_STORAGE_KEY);
  return (res && res.length && JSON.parse(res)) || [];
};

const CartSetItems = (items) => {
  localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(items));
};

const CartAddItem = (item) => {
  let now = CartGetItems();
  now.push(item);
  localStorage.setItem(CART_STORAGE_KEY, JSON.stringify(now));
};

const CartRemoveItem = (idx) => {
  let now = CartGetItems();
  now.splice(idx, 1);
  CartSetItems(now);
};

const CartClearAll = () => {
  localStorage.setItem(CART_STORAGE_KEY, JSON.stringify([]));
  let now = CartGetItems();
  CartSetItems(now);
};

export default {
  namespaced: true,
  state: () => ({
    cart: [],
  }),
  actions: {
    [ACTIONS.CART_ADD](ctx, item) {
      ctx.commit(MUTATIONS.CART_ADD, item);
      ctx.commit(MUTATIONS.CART_UPDATE);
    },
    [ACTIONS.CART_REMOVE](ctx, idx) {
      CartRemoveItem(idx);
      ctx.commit(MUTATIONS.CART_UPDATE);
    },
    CHANGE_ITEM_COUNT(ctx, { item, count }) {
      let now = CartGetItems(),
        t = now.find((in_cart) => in_cart.uid === item.uid);

      t.quantity = count;

      CartSetItems(now);

      ctx.commit(MUTATIONS.CART_UPDATE);
    },
  },
  mutations: {
    [MUTATIONS.CART_ADD]: (ctx, item) => {
      ctx.cart.push(item);
      CartAddItem(item);
    },
    [MUTATIONS.CART_REMOVE]: (ctx, idx) => {
      ctx.cart.splice(idx, 1);
      ctx.commit(MUTATIONS.CART_UPDATE);
    },
    [MUTATIONS.CART_CLEAR]: (ctx, item) => {
      ctx.cart = [];
      CartClearAll();
      // ctx.commit(MUTATIONS.CART_UPDATE);
    },
    [MUTATIONS.CART_UPDATE]: (ctx) => {
      ctx.cart = CartGetItems();
    },
  },
  getters: {
    user_cart: (state) => state.cart,
    user_cart_count: (state) => state.cart.length,
  },
};

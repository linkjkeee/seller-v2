import Vue from "vue";
import Vuex from "vuex";

import AuthModule from "./modules/Auth";
import InfoModule from "./modules/Info";
import ShipmentsModule from "./modules/Shipments";
import PaymentsModule from "./modules/Payments";
import AddressesModule from "./modules/Addresses";
import InvoicesModule from "./modules/Invoices";
import FinancesModule from "./modules/Finances";
import BlogModule from "./modules/Blog";
import ServiceModule from "./modules/ServiceModule";
import CdekModule from "./modules/Cdek";
import OrdersModule from "./modules/Orders";
import DisputesModule from "./modules/Disputes";
import CartModule from "./modules/Cart";
import NotificationsModule from "./modules/Notifications";
import UkrposhtaModule from "./modules/Ukrposhta";
import IntegrationsModule from "./modules/Integrations";
import ProductsModule from "./modules/Products";
import UktvedModule from "./modules/Uktved";
import WarehouseModule from "./modules/Warehouse";
import TwoFactorModule from "./modules/2fa";
import ServicesModule from "./modules/ServiceModule";
import CustomerServices from "./modules/CustomerServices";

Vue.use(Vuex);

const namespaced = true;

export default async function() {
  // export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      app: {
        namespaced,
        state: () => ({
          loading: {
            page: false,
            content: false,
          },
          lang: "ru",
        }),
        actions: {},
        mutations: {
          SET_LOADING: (state, loading) => {
            state.loading["content"] = loading;
          },
          SET_LANG: (state, lang) => {
            localStorage.setItem("sol-lang", lang);
            state.lang = lang;
          },
        },
        getters: {},
      },
      auth: AuthModule,
      Shipments: ShipmentsModule,
      Payments: PaymentsModule,
      Info: InfoModule,
      Addresses: AddressesModule,
      Invoices: InvoicesModule,
      Finances: FinancesModule,
      Blog: BlogModule,
      Service: ServiceModule,
      Cdek: CdekModule,
      Orders: OrdersModule,
      Disputes: DisputesModule,
      Cart: CartModule,
      Notifications: NotificationsModule,
      Ukrposhta: UkrposhtaModule,
      Integrations: IntegrationsModule,
      Products: ProductsModule,
      UKTVED: UktvedModule,
      Warehouse: WarehouseModule,
      "2fa": TwoFactorModule,
      Services: ServicesModule,
      CustomerServices,
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING,
  });

  return Store;
}

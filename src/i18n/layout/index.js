export const ru = {
  office_address: `
  Киев, Воздухофлотский проспект, 66<br />
  Новая почта, Киев, <b>отделение №18</b> <br />
  <br />
  <small>получатель:</small> <br />
  ООО «Атлас 2020»<br />
  +38-068-526-19-34
  `,

  notifications: {
    title: "уведомление | уведомления",
    settings: "Настройки уведомлений",
    read_all: "Прочитать все",
    remove_seen: "Удалить прочитанные",
    remove_all: "Удалить все",
    go_to_center: "Перейти в центр уведомлений",
    seen: "Прочитано",
    not_seen: "Не прочитано",
    load_more: "Загрузить ещё",
    thats_all: "это все уведомления на данный момент",
    no_notifications: "На данный момент у вас нет актуальных уведомлений",
    the_end: "это все уведомления на данный момент",

    actions: {
      delete_seen: "удалить прочитанные уведомления?",
      delete_all: "удалить все уведомления?",
      read_all: "прочитать все уведомления?",
    },
  },

  nav: {
    shipments: {
      _parent: "Посылки",
      new_shipment: {
        title: "Создать посылку",
        caption: "Создайте заявку на отправку посылки",
      },
      shipments: {
        title: "Мои посылки",
        caption: "Управляйте своими заявками",
      },
      warehouse: {
        title: "Товары на складе",
        caption: "Ваши товары на складе в США",
      },
      calc: {
        title: "Калькулятор",
        caption: "стоимости доставки",
      },
      replenishment: {
        title: "Пополнить счёт",
        caption: "Пополните счёт один из удобных для вас способом",
      },
      ukrposhta_list: {
        title: "Укрпошта: мои отправки",
      },
      ukrposhta_create: {
        title: "Укрпошта: создать отправку",
      },
      unidentified_returns: {
        title: "Неопознанные возвраты",
      },
      my_returns: {
        title: "Мои возвраты",
        caption: "Управление Вашими возвратами",
      },
    },
    bills: {
      _parent: "Счёт",
      my_bills: {
        title: "Мой счёт",
        caption: "Сводная информация о Вашем счёте",
      },
      money_in: {
        title: "Пополнить счёт",
        caption: "Пополнить счёт одним из удобных для вас способом",
      },
      money_out: {
        title: "Вывод остатков",
        caption: "Вывод остатков в долларах удобным для вас методом",
      },
    },
    payments: {
      _parent: "Платежи",
      payments_in: {
        title: "Входящие платежи",
        caption: "Ваши полученные платежи",
      },
      payments_out: {
        title: "Отправленные платежи",
        caption: "Ваши отправленные платежи",
      },
    },
    integrations: {
      _parent: "Интеграции",
      integrations: "интеграции",
      integrations_etsy: {
        title: "Интеграция Etsy",
      },
      integrations_amazon: {
        title: "Интеграция Amazon",
      },
      integrations_shopify: {
        title: "Интеграция Shopify",
      },
      integrations_ebay: {
        title: "Интеграция Ebay",
      },
      integrations_market: {
        title: "Интеграция с Маркетом",
      },
      integrations_market_old: {
        title: "Старый маркет",
      },
      other: {
        title: "Заявка на подключение магазина",
      },
    },
    account: {
      my_products: "Мои товары",
      personal_info: "Персональная информация",
      logout: "Выход",
      address_book: "Адресная книга",
      customs_codes_catalog: "Каталог кодов УКТВЭД",
      notifications_center: "Центр уведомлений",
      summary_info: "Сводная информация",
      settings: "Настройки",
      change_pass: "Изменить пароль",
      sales_settings: "Настройки продаж",
      subaccounts: "Ограниченный доступ для сотрудников",
      passport_info: "Паспортные данные",
      fop_contracts: "Договоры ФОП",
      two_factor_auth: "двухфакторная аутентификация",
    },
    my_account: {
      _parent: "Мой счёт",
      earn_payments: {
        title: "Принятые платежи",
      },
      sent_payments: {
        title: "Отправленные платежи",
      },
      refill_account: {
        title: "Пополнение счёта",
      },
      get_money: {
        title: "Вывод средств",
      },
      send_money: {
        title: "Перевод средств",
      },
      virtual_cards: {
        title: "Виртуальные карты",
      },
      payoneer_accounts: {
        title: "Аккаунты Payoneer",
      },
      cashback: {
        title: "Кешбэк",
      },
      bills: {
        title: "Выписанные счета",
      },
      balance_log: {
        title: "Журнал баланса",
      },
      hold_money_log: {
        title: "Журнал заморозки средств",
      },
    },
    buys: {
      _parent: "Покупки",
      new: {
        title: "Новая покупка",
      },
      my_buys: {
        title: "Мои покупки",
      },
      new_parcel: {
        title: "Новая посылка из США",
      },
      my_parcels: {
        title: "Мои посылки в США",
      },
      lost_orders: {
        title: "Неопознанные покупки",
      },
    },
    sells: {
      _parent: "Продажи",
      my_sells: {
        title: "Мои продажи",
      },
      personal_links: {
        title: "Персональные ссылки",
      },
      agent_sales: {
        title: "Продажи через посредника",
        products: {
          title: "Товары",
        },
        orders: {
          title: "Заказы",
        }
      },
      unidentified_payments: {
        title: "Неопознанные платежи",
      },
      disputes: {
        title: "Диспуты PayPal",
      },
    },
    home: "Главная",
    help: "Помощь",
  },
  balance: {
    waiting: "Ожидает",
    blocked: "Заблокировано",
    available: "Доступно",
    freezed: "Заморожено",
    will_remove: "Будет снято",
  },
  logo_slogan: "система покупок и продаж через интернет по всему миру",
  support: "поддержка",
  common_questions: "общие вопросы",
  office_address_label: "Адрес для отправки посылок",
  svh_address_label: "Фулфилмент склад в США",
  reciever: "получатель",
  send_feedback: "оставить отзыв",
  send_message: "написать письмо",
};
export const ua = {
  office_address: `
  Київ, просп. Повітрофлотський, 66<br />
  Нова пошта, Київ, <b>відділення №18</b><br />
  <br />
  <small>одержувач:</small> <br />
  ТОВ «Атлас 2020»<br />
  +38-068-526-19-34
  `,

  notifications: {
    title: "повідомлення | повідомлення",
    settings: "Налаштування повідомлень",
    read_all: "Прочитати все",
    remove_seen: "Видалити прочитані",
    remove_all: "Видалити все",
    go_to_center: "Перейти до центру сповіщень",
    seen: "Прочитано",
    not_seen: "Не прочитано",
    load_more: "Завантажити ще",
    thats_all: "це всі повідомлення на даний момент",
    no_notifications: "На даний момент у вас немає актуальних повідомлень",
    the_end: "це всі повідомлення на даний момент",

    actions: {
      delete_seen: "видалити прочитані повідомлення?",
      delete_all: "видалити всі повідомлення?",
      read_all: "прочитати всі повідомлення?",
    },
  },

  nav: {
    shipments: {
      _parent: "Відправлення",
      new_shipment: {
        title: "Створити посилку",
        caption: "Створіть заявку на надсилання посилки",
      },
      shipments: {
        title: "Мої посилки",
        caption: "Керуйте своїми заявками",
      },
      warehouse: {
        title: "Товари на складі",
        caption: "Ваші товари на складі у США",
      },
      calc: {
        title: "Калькулятор",
        caption: "вартості доставки",
      },
      replenishment: {
        title: "Поповнити рахунок",
        caption: "Поповніть рахунок одним з зручних для Вас способом",
      },
      ukrposhta_list: {
        title: "Укрпошта: мої відправлення",
      },
      ukrposhta_create: {
        title: "Укрпошта: створити відправлення",
      },
      unidentified_returns: {
        title: "Непізнанні повернення",
      },
      my_returns: {
        title: "Мої повернення",
        caption: "Управління Вашими поверненнями",
      },
    },
    bills: {
      _parent: "Рахунок",
      my_bills: {
        title: "Мой Рахунок",
        caption: "Зведена інформація про Ваш рахунок",
      },
      money_in: {
        title: "Поповнити рахунок",
        caption: "Поповнити рахунок одним из удобных для вас способом",
      },
      money_out: {
        title: "Вывод остатков",
        caption: "Вывод остатков в долларах удобным для вас методом",
      },
    },
    integrations: {
      _parent: "Інтеграції",
      integrations: "интеграции",
      integrations_etsy: {
        title: "Інтеграції Etsy",
      },
      integrations_amazon: {
        title: "Інтеграції Amazon",
      },
      integrations_shopify: {
        title: "Інтеграції Shopify",
      },
      integrations_ebay: {
        title: "Інтеграції Ebay",
      },
      integrations_market: {
        title: "Інтеграція з Маркетом",
      },
      integrations_market_old: {
        title: "Старий маркет",
      },
      other: {
        title: "Заявка на підключення магазину",
      },
    },
    account: {
      my_products: "Мої товари",
      personal_info: "Персональна інформація",
      logout: "Вихід",
      address_book: "Адресна книга",
      customs_codes_catalog: "Каталог кодів УКТЗЕД",
      notifications_center: "Центр сповіщень",
      summary_info: "Зведена інформація",
      settings: "Налаштування",
      change_pass: "Змінити пароль",
      sales_settings: "Налаштування продаж",
      subaccounts: "Обмежений доступ для працівників",
      passport_info: "Паспортні дані",
      fop_contracts: "Договори ФОП",
      two_factor_auth: "двофакторна автентифікація",
    },
    buys: {
      _parent: "Покупки",
      new: {
        title: "Нова покупка",
      },
      my_buys: {
        title: "Мої покупки",
      },
      new_parcel: {
        title: "Нова посилка з США",
      },
      my_parcels: {
        title: "Мої посилки у США",
      },
      lost_orders: {
        title: "Непізнані покупки",
      },
    },
    sells: {
      _parent: "Продажі",
      my_sells: {
        title: "Мої продажі",
      },
      personal_links: {
        title: "Персональні посилання",
      },
      agent_sales: {
        title: "Продажі через посередника",
        products: {
          title: "Товари",
        },
        orders: {
          title: "Замовлення",
        }
      },
      unidentified_payments: {
        title: "Непізнані платежі",
      },
      disputes: {
        title: "Диспути PayPal",
      },
    },
    my_account: {
      _parent: "Мій рахунок",
      earn_payments: {
        title: "Прийняті платежі",
      },
      sent_payments: {
        title: "Відправлені платежі",
      },
      refill_account: {
        title: "Поповнення рахунку",
      },
      get_money: {
        title: "Виведення коштів",
      },
      send_money: {
        title: "Переказ коштів",
      },
      virtual_cards: {
        title: "Віртуальні карти",
      },
      payoneer_accounts: {
        title: "Аккаунти Payoneer",
      },
      cashback: {
        title: "Кешбек",
      },
      bills: {
        title: "Виписані рахунки",
      },
      balance_log: {
        title: "Журнал балансу",
      },
      hold_money_log: {
        title: "Журнал заморозки коштів",
      },
    },
    home: "Головна",
    help: "Допомога",
  },
  balance: {
    waiting: "Очікує",
    blocked: "Заблоковано",
    available: "Доступно",
    freezed: "Заморожено",
    will_remove: "Буде знято",
  },
  logo_slogan: "система покупок та продажів через інтернет по всьому світу",
  support: "підтримка",
  common_questions: "загальні питання",
  office_address_label: "Адреса для відправки посилок",
  svh_address_label: "Фулфілмент склад в США",
  reciever: "одержувач",
  send_feedback: "залишити відгук",
  send_message: "надіслати листа",
};
export const en = {
  office_address: `
  Kyiv, Povitroflotski ave, 66<br />
  Nova Poshta, <b>office #18</b> <br />
  <br />
  <small>recipient:</small> <br />
  Atlas 2020 LLC<br />
  +38-068-526-19-34
  `,

  notifications: {
    title: "notification | notifications",
    settings: "Notifications settings",
    read_all: "Read all",
    remove_seen: "Remove seen",
    remove_all: "Remove all",
    go_to_center: "Go to notification center",
    seen: "Read",
    not_seen: "Unread",
    load_more: "Load more",
    thats_all: "these are all notifications for now",
    no_notifications: "You don't have any up-to-date notifications at the moment",
    the_end: "these are all notifications for now",

    actions: {
      delete_seen: "delete seen notifications?",
      delete_all: "delete all notifications?",
      read_all: "read all notifications?",
    },
  },

  nav: {
    shipments: {
      _parent: "Parcels",
      new_shipment: {
        title: "Create a Package",
        caption: "Create a request to send a parcel",
      },
      shipments: {
        title: "My packages",
        caption: "Manage your packages",
      },
      warehouse: {
        title: "Products in stock",
        caption: "Your items are in stock in the US",
      },
      calc: {
        title: "Calculator",
        caption: "of shipping costs",
      },
      replenishment: {
        title: "Replenish up account",
        caption: "Replenish up your account using one of the methods convenient for you",
      },
      ukrposhta_list: {
        title: "Ukrposhta: my shipments",
      },
      ukrposhta_create: {
        title: "Ukrposhta: create shipment",
      },
      unidentified_returns: {
        title: "Unidentified returns",
      },
      my_returns: {
        title: "My returns",
        caption: "Manage your returns",
      },
    },
    bills: {
      _parent: "Account",
      my_bills: {
        title: "My account",
        caption: "A summary info about your account",
      },
      money_in: {
        title: "Account replenishment",
        caption: "Replenish your account in one of the ways convenient for you",
      },
      money_out: {
        title: "Output of balances",
        caption: "Withdrawal of balances in dollars by a method convenient for you",
      },
    },
    payments: {
      _parent: "Payments",
      payments_in: {
        title: "Incoming Payments",
        caption: "Your received payments",
      },
      payments_out: {
        title: "Sent Payments",
        caption: "Your sent payments",
      },
    },
    integrations: {
      _parent: "Integrations",
      integrations: "integrations",
      integrations_etsy: {
        title: "Etsy Integration",
      },
      integrations_amazon: {
        title: "Amazon Integration",
      },
      integrations_shopify: {
        title: "Shopify Integration",
      },
      integrations_ebay: {
        title: "Ebay Integration",
      },
      integrations_market: {
        title: "SOL Market Integration",
      },
      integrations_market_old: {
        title: "Old market integration",
      },
      other: {
        title: "Application to connect the store",
      },
    },
    account: {
      my_products: "My products",
      personal_info: "Personal info",
      logout: "Logout",
      address_book: "Address book",
      customs_codes_catalog: "UKTVED codes catalog",
      notifications_center: "Notification center",
      summary_info: "Summary information",
      settings: "Settings",
      change_pass: "Change password",
      sales_settings: "Sales settings",
      subaccounts: "Restricted access for employees",
      passport_info: "Passport details",
      fop_contracts: "Entrepreneur contracts",
      two_factor_auth: "two-factor authentication",
    },
    my_account: {
      _parent: "My account",
      earn_payments: {
        title: "Incoming payments",
      },
      sent_payments: {
        title: "Sent payments",
      },
      refill_account: {
        title: "Account replenishment",
      },
      get_money: {
        title: "Withdrawals",
      },
      send_money: {
        title: "Transfer funds",
      },
      virtual_cards: {
        title: "Virtual cards",
      },
      payoneer_accounts: {
        title: "Payoneer accounts",
      },
      cashback: {
        title: "Cashback",
      },
      bills: {
        title: "Invoices",
      },
      balance_log: {
        title: "Balance log",
      },
      hold_money_log: {
        title: "Holded money log",
      },
    },
    buys: {
      _parent: "Purchases",
      new: {
        title: "New purchase",
      },
      my_buys: {
        title: "My purchases",
      },
      new_parcel: {
        title: "New package from USA",
      },
      my_parcels: {
        title: "My packages to the USA",
      },
      lost_orders: {
        title: "Unidentified purchases",
      },
    },
    sells: {
      _parent: "Sales",
      my_sells: {
        title: "My sales",
      },
      personal_links: {
        title: "Personal payment links",
      },
      agent_sales: {
        title: "Reseller sales",
        products: {
          title: "Products",
        },
        orders: {
            title: "Orders",
        }
      },
      unidentified_payments: {
        title: "Unidentified payments",
      },
      disputes: {
        title: "PayPal disputes",
      },
    },
    home: "home",
    help: "Help",
  },
  balance: {
    waiting: "Waiting",
    blocked: "Blocked",
    available: "Available",
    frozen: "Holded",
    will_remove: "Will be removed",
  },
  logo_slogan: "a system for buying and selling via the Internet around the world",
  support: "support",
  common_questions: "common questions",
  office_address_label: "send parcels to",
  svh_address_label: "Fulfillment warehouse in USA",
  receiver: "recipient",
  send_feedback: "leave feedback",
  send_message: "send email",
};

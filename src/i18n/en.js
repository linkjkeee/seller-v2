import { en } from "./pages/2fa";
import { en as agent_sales } from "./pages/agent-sales";
import { en as cashback } from "./pages/cashback";
import { en as dashboard } from "./pages/dashboard";
import { en as disputes } from "./pages/disputes";
import { en as fee_cards } from "./pages/fee-cards";
import { en as get_money } from "./pages/get-money";
import { en as lost_orders } from "./pages/lost-orders";
import { en as new_buy } from "./pages/new-buy";
import { en as pay_money } from "./pages/pay-money";
import { en as payment_links } from "./pages/payment-links";
import { en as payments } from "./pages/payments";
import { en as send_money } from "./pages/send-money";
import { en as settings } from "./pages/settings";
import { en as shipments } from "./pages/shipments";
import { en as integrations } from "./pages/integrations";
import { en as lost_payments } from "./pages/lost-payments";
import { en as import_products } from "./pages/import-products";
import { en as call } from "./pages/call";
import { en as services } from "./pages/services";
import { en as crm } from "./pages/crm";
import { en as warehouse } from "./pages/warehouse";
import { en as fop } from "./pages/fop";

import { en as common } from "./common/index.js";
import { en as errors } from "./errors/index.js";
import { en as layout } from "./layout/index.js";
import { en as register } from "./pages/register";

export default {
  ...common,
  ...layout,
  errors,
  pages: {
    "2fa": en,
    agent_sales,
    cashback,
    dashboard,
    disputes,
    fee_cards,
    get_money,
    lost_orders,
    new_buy,
    pay_money,
    payment_links,
    payments,
    send_money,
    settings,
    shipments,
    integrations,
    lost_payments,
    import_products,
    call,
    services,
    crm,
    register,
    warehouse,
    fop,
  },
};

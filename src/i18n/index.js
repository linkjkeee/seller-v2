import en from "./en.js";
import uk from "./ua.js";
import ru from "./ru.js";

export default {
  ru,
  uk,
  en,
  // "en-US": en,
};

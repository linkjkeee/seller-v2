import { ua } from "./pages/2fa";
import { ua as agent_sales } from "./pages/agent-sales";
import { ua as cashback } from "./pages/cashback";
import { ua as dashboard } from "./pages/dashboard";
import { ua as disputes } from "./pages/disputes";
import { ua as fee_cards } from "./pages/fee-cards";
import { ua as get_money } from "./pages/get-money";
import { ua as lost_orders } from "./pages/lost-orders";
import { ua as new_buy } from "./pages/new-buy";
import { ua as pay_money } from "./pages/pay-money";
import { ua as payment_links } from "./pages/payment-links";
import { ua as payments } from "./pages/payments";
import { ua as send_money } from "./pages/send-money";
import { ua as settings } from "./pages/settings";
import { ua as shipments } from "./pages/shipments";
import { ua as integrations } from "./pages/integrations";
import { ua as import_products } from "./pages/import-products";
import { ua as call } from "./pages/call";
import { ua as services } from "./pages/services";
import { ua as crm } from "./pages/crm";
import { ua as warehouse } from "./pages/warehouse";
import { ua as fop } from "./pages/fop";

import { ua as common } from "./common/index.js";
import { ua as errors } from "./errors/index.js";
import { ua as layout } from "./layout/index.js";
import { ua as lost_payments } from "./pages/lost-payments";

import { ua as register } from "./pages/register";

export default {
  ...common,
  ...layout,
  errors,
  pages: {
    "2fa": ua,
    agent_sales,
    cashback,
    dashboard,
    disputes,
    fee_cards,
    get_money,
    lost_orders,
    new_buy,
    pay_money,
    payment_links,
    payments,
    send_money,
    settings,
    shipments,
    integrations,
    lost_payments,
    import_products,
    call,
    services,
    crm,
    register,
    warehouse,
    fop,
  },
};

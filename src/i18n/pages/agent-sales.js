export const ru = {
  product_links: "Ссылка на товар | Ссылки на товары",
  on_market: "на торговой площадке | на торговых площадках",
  add_link: "добавить ссылку",
  personalization_help: "Инструкция по персонализации",
  manager_phone: "Телефон для связи с менеджером",
  pick_materials: "перечислите материалы через запятую",
  can_change_product: "Вы можете изменить этот товар",
  no_changes_allowed: "В данном статусе заявки нельзя внести изменения",
  your_requests: "ваши заявки",
  in_stock: "остаток на складе",
  drive_folder_link: "Ссылка на папку Google Drive",
  main_image: "Основное изображение",
  additional_images: "другие изображения",
  agent_product: "Товар у посредника",
  request_number: "Номер заявки",
  your_product: "Ваш товар",

  request_id_asc: "Заявка: от старых к новым",
  request_id_desc: "Заявка: от новых к старым",
  product_stock_asc: "Остаток: по возрастанию",
  product_stock_desc: "Остаток: по убыванию",

  product_status_filter: {
    title: "Статус товара",
    in_warehouse: "Есть на складе",
    on_the_way: "В пути",
  },

  status_active: "Активные",
  status_completed: "Выполненные",

  messages: {
    drive_folder_help:
      "Вы можете загрузить серию фотографий Вашего товара на Google Drive и использовать их для оформления заявки",
    google_drive_warning: "Значение должно быть ссылкой на папку Google Drive",
    link_product: "Вы можете связать данную заявку с Вашим товаром в системе, для этого выберите его из своих товаров",
    materials_hint: "перечислите материалы через запятую",
    if_has_parcel_in_system: "в системе, если воспользовались нашей доставкой до склада",
    in_case_self_delivery: "в случае самостоятельной отправки на склад",
    additional_delivery_info: "Дополнительная информация о доставке",
    info:
      "На этой странице вы можете увидеть список заявок на продажу своего товара через посредника или создать новую заявку. Редактирование заявки доступно только до тех пор, пока она находится на рассмотрении у операторов. В случае одобрения или отклонения заявки вы получите автоматическое уведомление.",
    orders_info: "На этой странице вы можете увидеть список заказов, которые были оформлены в магазине посредника по вашим заявкам. Отображаются только товары, которые еще не отправлены.",
    orders_info_warning: "Это экспериментальная функция, некоторые заказы могут появиться в списке позже",
  },


};

export const ua = {
  product_links: "посилання на товар | посилання на товари",
  on_market: "на торговому майданчику | на торгових майданчиках",
  add_link: "додати посилання",
  personalization_help: "Інструкція з персоналізації",
  manager_phone: "Телефон для зв'язку з менеджером",
  pick_materials: "перерахуйте матеріали через кому",
  can_change_product: "Ви можете змінити цей товар",
  no_changes_allowed: "У цьому статусі заявки не можна змінити",
  your_requests: "ваші заявки",
  in_stock: "залишок на складі",
  drive_folder_link: "Посилання на папку Google Drive",
  main_image: "Основне зображення",
  additional_images: "інші зображення",
  agent_product: "Товар у посередника",
  request_number: "Номер заявки",
  your_product: "Ваш товар",

  request_id_asc: "Заявка: від старих до нових",
  request_id_desc: "Заявка: від нових до старих",
  product_stock_asc: "Залишок: за зростанням",
  product_stock_desc: "Залишок: за спаданням",

  product_status_filter: {
    title: "Статус товару",
    in_warehouse: "Є на складі",
    on_the_way: "В дорозі",
  },

  status_active: "Активні",
  status_completed: "Виконані",

  messages: {
    drive_folder_help:
      "Ви можете завантажити серію фотографій Вашого товару на Google Drive та використовувати їх для оформлення заявки",
    google_drive_warning: "Значення має бути посиланням на папку Google Drive",
    link_product: "Ви можете зв'язати цю заявку з Вашим товаром у системі, для цього оберіть його зі своїх товарів",
    materials_hint: "перерахуйте матеріали через кому",
    if_has_parcel_in_system: "в системі, якщо скористалися нашою доставкою до складу",
    in_case_self_delivery: "у разі самостійного відправлення на склад",
    additional_delivery_info: "Додаткова інформація про доставку",
    info:
      "На цій сторінці ви можете побачити список заявок на продаж свого товару через посередника або створити нову заявку. Редагування заявки доступне лише доти, доки вона перебуває на розгляді операторів. У разі схвалення або відхилення заявки ви отримаєте автоматичне повідомлення.",
    orders_info: "На цій сторінці ви можете побачити список замовлень, які були оформлені в магазині посередника за вашими заявками. Відображаються лише товари, які ще не відправлені.",
    orders_info_warning: "Це експериментальна функція, деякі замовлення можуть з'явитися в списку пізніше",
  },
};

export const en = {
  product_links: "Product Link | Product Links",
  on_market: "on the marketplace | on the marketplaces",
  add_link: "add link",
  personalization_help: "Personalization guide",
  manager_phone: "Manager phone",
  pick_materials: "list materials separated by commas",
  can_change_product: "You can change this product",
  no_changes_allowed: "Changes cannot be made in this ticket status",
  your_requests: "your requests",
  in_stock: "in stock",
  drive_folder_link: "Link to Google Drive folder",
  main_image: "Main image",
  additional_images: "additional images",
  agent_product: "Agent product",
  request_number: "Request number",
  your_product: "Your product",

  request_id_asc: "Request: old to new",
  request_id_desc: "Request: new to old",
  product_stock_asc: "Stock: less to more",
  product_stock_desc: "Stock: more to less",

  product_status_filter: {
    title: "Product status",
    in_warehouse: "In stock",
    on_the_way: "On the way",
  },

  status_active: "Active",
  status_completed: "Completed",

  messages: {
    drive_folder_help:
      "You can upload a series of photos of your product to Google Drive and use them to complete the request",
    google_drive_warning: "The value must be a link to a Google Drive folder",
    link_product: "You can link this request with your product in the system, to do this, select it from your products",
    materials_hint: "list materials separated by commas",
    if_has_parcel_in_system: "in the system, if you have used our delivery to the warehouse",
    in_case_self_delivery: "in case of self-delivery to the warehouse",
    additional_delivery_info: "Additional delivery information",
    info:
      "On this page, you can see a list of requests for the sale of your product through an intermediary or create a new request. Editing the request is available only as long as it is under consideration by the operators. If your request is approved or rejected, you will receive an automatic notification.",
    orders_info: "On this page, you can see a list of orders that were placed in the agent's store for your requests. Only products that have not yet been shipped are displayed.",
    orders_info_warning: "This is an experimental feature, some orders may appear in the list later",
  },
};

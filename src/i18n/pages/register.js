export const ru = {
  success_register_message: "Вы успешно зарегистрировались",
  accept_agreement_warn:
    "Для подолжения регистрации вы должны согласиться с условиями пользовательского соглашения",
  register_password_hint:
    "Пароль выслан Вам на указанную почту. Используйте его для входа в систему. Вы можете сменить пароль в любое время",
  auto_create: "Создаётся автоматически",
  agreement: "Пользовательское соглашение",
  agreement_message: "Внимательно прочтите текст пользовательского соглашения",
  pick_user_type: "Выберите тип деятельности",
  user: "физическое лицо",
  flp: "Физическое лицо-предприниматель (ФЛП)",
  lcc: "Общество с ограниченной ответственностью (ООО)",
  register: "Зарегистрироваться",
  accept_agreement_message: `Отправляя свои данные на модерацию и создавая учетную запись Вы подтверждаете,
  что ознакомились с
  <a @click.stop href="https://seller-online.com/user-agreement/" target="_blank">
    текстом публичной пользовательского соглашения </a
  >, и в случае, если Вы не согласны с каким-либо пунктом Договора-оферты, Вам
  необходимо отказаться от дальнейшей регистрации в сервисе и не осуществлять
  использование наших услуг.`,

  psrn: "ЕГРПОУ",
  request_received: "Ваш запрос принят в обработку",

};

export const ua = {
  success_register_message: "Ви успішно зареєструвалися",
  accept_agreement_warn: "Для реєстрації Ви повинні погодитися з умовами Договору-оферти",
  register_password_hint:
    "Пароль надіслано Вам на вказану пошту. Використовуйте його для входу в систему. Ви можете змінити пароль у будь-який час",
  auto_create: "Створюється автоматично",
  agreement: "Користувацька угода",
  agreement_message: "Уважно прочитайте текст користувацької угоди",
  pick_user_type: "Оберіть тип діяльності",
  user: "фізична особа",
  flp: "Фізична особа-підприємець (ФОП)",
  lcc: "Товариство з обмеженою відповідальністю (ТОВ)",
  register: "Зареєструватися",
  accept_agreement_message: `Надсилаючи свої дані на модерацію та створюючи обліковий запис Ви підтверджуєте,
  що ознайомилися з
  <a @click.stop href="https://seller-online.com/user-agreement/" target="_blank">
    текстом користувацької угоди </a
  >, і у випадку, якщо Ви не погоджуєтесь з будь-яким пунктом Договору-оферти, Вам
  необхідно відмовитися від подальшої реєстрації у сервісі та не здійснювати
  використання наших послуг.`,

  psrn: "ЄДРПОУ",
  request_received: "Ваш запит прийнято до обробки",
};
export const en = {
  success_register_message: "You have successfully registered",
  accept_agreement_warn:
    "To continue registration, you must agree to the terms of the user agreement",
  register_password_hint:
    "The password has been sent to your email address. Use it to log in. You can change your password at any time",
  auto_create: "Will create automatically",
  agreement: "User agreement",
  agreement_message: "Please read the user agreement carefully",
  pick_user_type: "Select activity type",
  user: "individual",
  flp: "Private entrepreneur",
  lcc: "LLC",
  register: "Register",
  accept_agreement_message: `By submitting your details for moderation and creating an account, you acknowledge that
  that got acquainted with
  <a @click.stop href="https://seller-online.com/user-agreement/" target="_blank">
    the text of the user agreement</a
  >, and if you do not agree with any clause of the user agreement, you
  it is necessary to refuse further registration in the service and not to carry out
  use of our services.`,

  psrn: "Company ID",
  request_received: "Your request has been received",
};

export const ru = {
  refill: "пополнения",
  req_message: "При неверном заполнении дополнительной информации заявка на пополнение счета может быть отклонена",
};
export const ua = {
  refill: "refills",
  req_message: "При неправильному заповненні додаткової інформації заявка на поповнення рахунку може бути відхилена",
};
export const en = {
  refill: "refills",
  req_message: "The request for account replenishment may be rejected if additional information is filled incorrectly",
};

export const ru = {
  etsy: {
    v3_warning: `
    Обратите внимание,
    <b>что с 1 января 2022 года интеграции старого образца (v2) перестанут работать</b>.
    <br />
    Чтобы продолжить использование автоматических инструментов Etsy, обновите существующие интеграции до версии v3.
    Информация о статусе интеграций для подключенных магазинов представлена ниже.
    <div>
      <b>
        Активные интеграции v2 удалять не нужно, просто пройдите для этих магазинов новую интеграцию на этой
        странице.
      </b>
    </div>
    `,
    integration_info: `Интеграция с Etsy позволит в автоматическом режиме (без использования пароля и Ваших персональных данных для входа в магазин) получать данные о заказе и добавлять номера почтовых отправлений в заказы в Etsy`,
    integration_help: "инструкция по интеграции",

    integration_faq_title: "Интеграция с Etsy предназначена для:",
    integration_faq: [
      "Получения деталей заказа в форму для отображения платежа (состав заказа, сроки изготовления, сведения о получателе).",
      "Быстрого оформления заявки на отправку посылки (в строке поиска можно использовать, как номер заказа-продажи из Seller-Online, так и можно делать поиск по Etsy order).",
      "Автоматического импорта треков и заказов для внутренней CRM.",
      {
        title: "Автоматической загрузки трек-номеров на Etsy, которые выдаются:",
        items: [
          "службой доставки Seller-Online",
          "Укрпоштой через создание документов на отправку в личном кабинете (после фактической отправки в отделении)",
          "при самостоятельном добавлении к заказу трек-номера (если этот заказ был получен через Etsy)",
        ],
      },
    ],

    add_new_etsy_shop: "Подключение нового магазина Etsy",
    add_new_etsy_info: `
        Введите название магазина Etsy, который вы хотите подключить, и нажмите кнопку
        <span class="font-500">"@pages.integrations.gen_link"</span>.
    `,

    change_shop_name_info: `
        В случае изменения имени магазина, не забудьте после этого снова нажать кнопку
        <span class="text-blue font-500">"@pages.integrations.gen_link"</span>.
    `,
    etsy_shop_title: "Название магазина Etsy",
    redirect_info: `
        После нажатия на кнопку ниже вы будете перенаправлены в свой магазин Etsy для подтверждения установки
        приложения <b>Seller-Online</b>.
        <br />
        <br />
        Для продолжения нажмите на кнопку "Подключить магазин {{ current_shop }}" и следуйте инструкциям (вам может
        понадобиться авторизация в своем магазине):
    `,
  },

  amazon: {
    statuses: {
      complete: "Настроен",
      uncomplete: "Не настроен",
    },

    connected_amazon_shops: "Ваши подключенные магазины Amazon",
    is_unavailable: "В данный момент запросы по API Amazon недоступны. Мы работаем над решением проблемы. Приносим извинения за неудобства.",

    get_token_title: "Получение Seller ID и MWS Auth token",
    get_token_help:
      "Для получения <b>Seller ID</b> и <b>MWS Auth</b> token используйте следующие данные:",
  },

  shopify: {
    copyright:
      "Термин 'Shopify' является зарегистрированной торговой маркой компании Shopify Inc.",
    actual_rules: "Актуальные условия использования сервиса",

    rules_confirm: `
      Интеграция происходит через публичное приложение
      <b>Seller-Online Connect</b>, разработанного с учетом
      <a href="https://www.shopify.com/legal/api-terms">
        правил использования API Shopify
      </a>
    `,

    integration_help: {
      title: "Интеграция с Shopify предназначена для:",
      items: [
        "Быстрого оформления заявки на отправку посылки (в строке поиска можно использовать номер заказа Shopify)",
        "Автоматического определения таможенной стоимости исходя из суммы заказа.",
      ],
    },

    new_shop: "Подключение нового магазина Shopify",
    change_shop_warn: [
      `В случае изменения имени магазина, не забудьте после этого снова нажать кнопку "@gen_link".`,
      `Введите название магазина Shopify, который вы хотите подключить, и нажмите кнопку "@gen_link".`,
    ],

    finish_connect: [
      `После нажатия на кнопку ниже вы будете перенаправлены в свой магазин Shopify для подтверждения установки приложения <b>Seller-Online Connect</b>.`,
      `Для продолжения нажмите на кнопку "Подключить магазин`,
      `" и следуйте инструкциям (вам может понадобиться авторизация в своем магазине):`,
    ],

    integrated_shops: "Интегрированные магазины Shopify",
  },

  ebay: {
    copyright: "Термин 'eBay' является зарегистрированной торговой маркой компании eBay Inc.",
    actual_rules: "Актуальные условия использования сервиса",

    help_info: `
    Интеграция происходит через публичное приложение
    <b>Seller-Online Connect</b>, разработанного на основе API eBay.
    `,

    help_title: "Интеграция с eBay предназначена для:",
    help_items: [
      "Быстрого оформления заявки на отправку посылки (в строке поиска можно использовать номер заказа eBay)",
      "Автоматического определения таможенной стоимости исходя из суммы заказа.",
    ],

    connect_new_ebay: "Подключение нового магазина eBay",
    gen_link_help: `Введите имя пользователя (логин) или почту, которые вы используете для авторизации на eBay, и нажмите кнопку "Сгенерировать ссылку".`,
    change_shop_help: `В случае изменения имени магазина, не забудьте после этого снова нажать кнопку "Сгенерировать ссылку".`,

    finish_connect: [
      `После нажатия на кнопку ниже вы будете перенаправлены в свой аккаунт eBay для подтверждения установки приложения <b>Seller-Online eBay Connect</b>.`,
      `Для продолжения нажмите на кнопку "Подключить магазин" и следуйте инструкциям (вам может понадобиться авторизация в своем магазине):`,
    ],

    connected_ebay_shops: "Список интегрированных магазинов eBay",
    update_before: "Обновить до",
    outdate: "Истекает",
  },

  market: {
    connected_shops: "Список интегрированных магазинов Seller-Online Market",
    connect: "Создать магазин",
  },

  unlink_pompt:
    "Отвязать аккаунт?\nВ будущем Вам придется вновь пройти интеграцию, чтобы работать с этим магазином.",
  configure: "настройка",

  track_import_settings_title: " Настройка автоматической загрузки треков на Etsy",
  track_import_settings_info: `
    Выберите наиболее подходящий для вас способ автоматической загрузки трек-номеров консолидаций. Если вы не хотите,
    чтобы мы отправляли трек-номера, присвоенные вашим посылкам, то выберите соответствующую опцию.
    `,

  track_import_settings: "Настройки отправки треков",
  gen_link: "Сгенерировать ссылку",
  connect_shop: "Подключить магазин",
  integrated_shops_yet: "Ранее интегрированные магазины",
  sales_from_not_connected_title: "Продажи из неподключенных магазинов.",
  sales_from_not_connected_desc:
    "Магазины, по которым были продажи, но которые еще не интегрированы с Seller-Online.",

  track_import_settings_options: {
    no_send: {
      title: "Не отправлять",
      desc: "Мы не будем отправлять трек-номера в Etsy. Вам нужно будет вносить их вручную",
    },
    only_destination: {
      title: "Отправлять трек",
      desc: "Отправляется трек, по которому посылка доставляется покупателю",
    },
    destination_after_us: {
      title: "Отправить трек после обработки в США",
      desc:
        "Трек отправляется в Etsy после того, как посылка принята и обработана на складе в США",
    },
  },

  other: {
    no_requests: "Вы не создали ни одной заявки",
    description: `Настройка приема платежей с различных интернет-маркетов требует добавления прав для Вашего
      магазина в нашем PayPal счете. Для подключения заполните, пожалуйста, все необходимые поля и
      создайте заявку. Ваша заявка будет обработана в течение рабочего дня. После обработки заявки
      Вам на почту придет уведомление.`,
  }
};
export const ua = {
  etsy: {
    v3_warning: `
        Зверніть увагу,
        <b>що з 1 січня 2022 року інтеграції старого зразка (v2) перестануть працювати</b>.
        <br />
        Щоб продовжити використання автоматичних інструментів Etsy, оновіть наявні інтеграції до версії v3.
        Інформація про статус інтеграцій для підключених магазинів наведена нижче.
        <div>
            <b>
            Активні інтеграції v2 видаляти не потрібно, просто пройдіть для цих магазинів нову інтеграцію на цій
            сторінці.
            </b>
        </div>
    `,
    integration_info: `Інтеграція з Etsy дозволить в автоматичному режимі (без використання пароля та Ваших персональних даних для входу в магазин) отримувати дані про замовлення та додавати номери поштових відправлень у замовлення до Etsy`,
    integration_help: "інструкція з інтеграції",

    integration_faq_title: "Інтеграція з Etsy призначена для:",
    integration_faq: [
      "Отримання деталей замовлення у форму для відображення платежу (склад замовлення, терміни виготовлення, відомості про одержувача).",
      "Швидкого оформлення заявки на відправку посилки (у рядку пошуку можна використовувати як номер замовлення-продажу з Seller-Online, так і можна робити пошук по Etsy order).",
      "Автоматичного імпорту треків та замовлень для внутрішньої CRM.",
      {
        title: "Автоматичного завантаження трек-номерів на Etsy, які видаються:",
        items: [
          "службою доставки Seller-Online",
          "Укрпоштою через створення документів на відправку в особистому кабінеті (після фактичного відправлення у відділенні)",
          "при самостійному додаванні до замовлення трек-номера (якщо це замовлення було отримано через Etsy)",
        ],
      },
    ],
    add_new_etsy_shop: "Підключення нового магазину Etsy",
    add_new_etsy_info: `
        Введіть назву магазину Etsy, який ви хочете підключити, та натисніть кнопку
        <span class="font-500">"Створити посилання"</span>.
    `,
    change_shop_name_info: `
        У разі зміни імені магазину, не забудьте після цього знову натиснути кнопку
        <span class="text-blue font-500">"Сгенерувати посилання"</span>.
    `,
    etsy_shop_title: "Назва магазину Etsy",
    redirect_info: `
        Після натискання на кнопку нижче ви будете перенаправлені до свого магазину Etsy для підтвердження установки
        програми <b>Seller-Online</b>.
        <br />
        <br />
        Для продовження натисніть на кнопку "Підключити магазин {{ current_shop }}" і дотримуйтесь інструкцій (вам може
        знадобиться авторизація у своєму магазині):
    `,
  },

  shopify: {
    copyright: "Термін 'Shopify' є зареєстрованою торговою маркою компанії Shopify Inc.",
    actual_rules: "Актуальні умови використання сервісу",

    rules_confirm: `
      Інтеграція відбувається через публічний додаток
      <b>Seller-Online Connect</b>, розробленого з урахуванням
      <a href="https://www.shopify.com/legal/api-terms">
        правил використання API Shopify
      </a>
    `,

    integration_help: {
      title: "Інтеграція з Shopify призначена для:",
      items: [
        "Швидкого оформлення заявки на відправку посилки (у рядку пошуку можна використовувати номер замовлення Shopify)",
        "Автоматичного визначення митної вартості виходячи із суми замовлення.",
      ],
    },

    new_shop: "Підключення нового магазину Shopify",

    change_shop_warn: [
      `У разі зміни імені магазину, не забудьте після цього знову натиснути кнопку "@gen_link".`,
      `Введіть назву магазину Shopify, який ви хочете підключити, та натисніть кнопку "@gen_link".`,
    ],

    finish_connect: [
      `Після натискання на кнопку нижче ви будете перенаправлені до свого магазину Shopify для підтвердження установки програми <b>Seller-Online Connect</b>.`,
      `Для продовження натисніть кнопку "Підключити магазин"`,
      `" і дотримуйтесь інструкцій (вам може знадобитися авторизація у своєму магазині):`,
    ],

    integrated_shops: "Інтегровані магазини Shopify",
  },

  amazon: {
    is_unavailable: "В даний момент запити по API Amazon недоступні. Ми працюємо над вирішенням проблеми. Просимо вибачення за незручності.",
  },

  ebay: {
    copyright: "Термін 'eBay' є зареєстрованою торговою маркою компанії eBay Inc.",
    actual_rules: "Актуальні умови використання сервісу",

    help_info: `
    Інтеграція відбувається через публічний додаток
    <b>Seller-Online Connect</b>, розробленого на основі API eBay.
    `,

    help_title: "Інтеграція з eBay призначена для:",
    help_items: [
      "Швидкого оформлення заявки на відправку посилки (у рядку пошуку можна використовувати номер замовлення eBay)",
      "Автоматичного визначення митної вартості виходячи із суми замовлення.",
    ],

    connect_new_ebay: "Підключення нового магазину eBay",
    gen_link_help: `Введіть ім'я користувача (логін) або пошту, які ви використовуєте для авторизації на eBay, та натисніть кнопку "Сгенерувати посилання".`,
    change_shop_help: `У разі зміни імені магазину не забудьте після цього знову натиснути кнопку "Сгенерувати посилання".`,

    finish_connect: [
      `Після натискання на кнопку нижче ви будете перенаправлені в свій обліковий запис eBay для підтвердження установки програми <b>Seller-Online eBay Connect</b>.`,
      `Для продовження натисніть на кнопку "Підключити магазин" і дотримуйтесь інструкцій (вам може знадобитися авторизація у своєму магазині):`,
    ],

    connected_ebay_shops: "Список інтегрованих магазинів eBay",
    update_before: "Оновити до",
    outdate: "Мир",
  },

  market: {
    connected_shops: "Список інтегрованих магазинів Seller-Online Market",
    connect: "Створити магазин",
  },

  unlink_pompt:
    "Відв'язати акаунт?\nУ майбутньому Вам доведеться знову пройти інтеграцію, щоб працювати з цим магазином.",

  track_import_settings_title: "Налаштування автоматичного завантаження треків на Etsy",
  track_import_settings_info: `
    Оберіть найбільш вдалий спосіб автоматичного завантаження трек-номерів консолідацій. Якщо ви не хочете,
    щоб ми надсилали трек-номери, присвоєні вашим посилкам, Оберіть відповідну опцію.
    `,
  track_import_settings: "Налаштування надсилання треків",
  gen_link: "створити посилання",
  connect_shop: "Підключити магазин",
  integrated_shops_yet: "Раніше інтегровані магазини",
  sales_from_not_connected_title: "Продажі з непідключених магазинів.",
  sales_from_not_connected_desc:
    "Покупки, за якими були продажі, але які ще не інтегровані з Seller-Online.",

  track_import_settings_options: {
    no_send: {
      title: "Не надсилати",
      desc: "Ми не надсилатимемо трек-номери в Etsy. Вам потрібно буде вносити їх вручну",
    },
    only_destination: {
      title: "Відправляти трек",
      desc: "Відправляється трек, яким посилка доставляється покупцю",
    },
    destination_after_us: {
      title: "Надіслати трек після обробки в США",
      desc:
        "Трек відправляється в Etsy після того, як посилка прийнята та оброблена на складі в США",
    },
  },

  other: {
    no_requests: "Ви не створили жодної заявки",
    description: `Налаштування прийому платежів з різних інтернет-магазинів потребує додавання прав для Вашого
        магазину в нашому PayPal рахунку. Для підключення заповніть, будь ласка, всі необхідні поля та створіть
        заявку. Ваша заявка буде оброблена протягом робочого дня. Після обробки заявки Вам на пошту прийде
        повідомлення.`,
  }
};
export const en = {
  etsy: {
    v3_warning: `
    Note,
    <b>that as of January 1, 2022, old-style integrations (v2) will no longer work</b>.
    <br />
    To continue using Etsy's automated tools, please update your existing integrations to v3.
    Information about the status of integrations for connected stores is presented below.
    <div>
      <b>
        Active v2 integrations do not need to be deleted, just go through the new integration for these stores on this
        page.
      </b>
    </div>
    `,
    integration_info: `Integration with Etsy will allow you to automatically (without using a password and your personal data to enter the store) receive order data and add postage numbers to orders in Etsy`,
    integration_help: "integration instructions",

    integration_faq_title: "Etsy integration is for:",
    integration_faq: [
      "Receiving order details in the form for displaying payment (order content, production time, information about the recipient).",
      "Fast processing of an application for sending a parcel (in the search bar you can use both the sales order number from Seller-Online, or you can search by Etsy order).",
      "Automatic import of tracks and orders for internal CRM.",
      {
        title: "Automatically upload tracking numbers to Etsy that are given out:",
        items: [
          "Seller-Online delivery service",
          "Ukrposhta through the creation of documents for sending in your account (after the actual sending in the office)",
          "when adding a tracking number to the order on your own (if this order was received via Etsy)",
        ],
      },
    ],

    add_new_etsy_shop: "Connecting a New Etsy Shop",
    add_new_etsy_info: `
        Enter the name of the Etsy store you want to connect and click the button
        <span class="font-500">"Generate link"</span>.
    `,

    change_shop_name_info: `
        If you change the name of the store, do not forget to press the button again after that.
        <span class="text-blue font-500">"Generate link"</span>.
    `,
    etsy_shop_title: "Etsy shop title",
    redirect_info: `
        After clicking on the button below, you will be redirected to your Etsy store to confirm the installation
        <b>Seller-Online</b> applications.
        <br />
        <br />
        To continue, click on the "Connect shop {{ current_shop }}" button and follow the instructions (you may
        you need authorization in your store):
    `,
  },

  shopify: {
    copyright: "'Shopify' is a registered trademark of Shopify Inc.",
    actual_rules: "Actual terms of service use",

    rules_confirm: `
      Integration occurs through a public application
      <b>Seller-Online Connect</b>, designed with
      <a href="https://www.shopify.com/legal/api-terms">
        Shopify API Terms of Service
      </a>
    `,

    integration_help: {
      title: "Shopify Integration is for:",
      items: [
        "Fast application for sending a package (you can use the Shopify order number in the search bar)",
        "Automatic determination of the customs value based on the amount of the order.",
      ],
    },
    new_shop: "Connecting a new Shopify store",

    change_shop_warn: [
      `If you change the name of the store, do not forget to click the "@gen_link" button again after that.`,
      `Enter the name of the Shopify store you want to connect and click the "@gen_link" button.`,
    ],

    finish_connect: [
      `After clicking on the button below, you will be redirected to your Shopify store to confirm the installation of the <b>Seller-Online Connect</b>.`,
      `To continue, click on the "Connect Store" button`,
      `" and follow the instructions (you may need to log into your store):`,
    ],

    integrated_shops: "Integrated Shopify Stores",
  },

  ebay: {
    copyright: "The term 'eBay' is a registered trademark of eBay Inc.",
    actual_rules: "Actual terms of service use",

    help_info: `
    Integration occurs through a public application
    <b>Seller-Online Connect</b> based on the eBay API.
    `,

    help_title: "eBay integration is for:",
    help_items: [
      "Fast application for sending a parcel (you can use the eBay order number in the search bar)",
      "Automatic determination of the customs value based on the amount of the order.",
    ],

    connect_new_ebay: "Connecting a new eBay store",
    gen_link_help: `Enter the username (login) or email that you use to log in to eBay, and click the "Generate Link" button.`,
    change_shop_help: `If you change the shop name, don't forget to click the "Generate link" button again afterwards.`,

    finish_connect: [
      `After clicking the button below, you will be redirected to your eBay account to confirm the installation of the <b>Seller-Online eBay Connect</b>.`,
      `To continue, click on the "Connect Store" button and follow the instructions (you may need to log in to your store):`,
    ],

    connected_ebay_shops: "List of connected eBay stores",
    update_before: "Update before",
    update: "Expires",
  },

  market: {
    connected_shops: "List of integrated Seller-Online Market shops",
    connect: "Create shop",
  },

  unlink_pompt:
    "Unlink your account?\nIn the future you will have to go through the integration again to work with this store.",

  amazon: {
    statuses: {
      complete: "Configured",
      uncomplete: "Not configured",
    },

    connected_amazon_shops: "Your connected Amazon stores",
    is_unavailable: "At the moment, Amazon API requests are not available. We are working on a solution to the problem. We apologize for the inconvenience.",


    get_token_title: "Getting Seller ID and MWS Auth token",
    get_token_help: "To get <b>Seller ID</b> and <b>MWS Auth</b> token use the following data:",
  },

  track_import_settings_title: "Setting Up Automatic Track Uploads on Etsy",
  track_import_settings_info: `
    Choose the most suitable way for you to automatically download the tracking numbers of consolidations. If you do not want,
    so that we send the track numbers assigned to your parcels, then select the appropriate option.
    `,

  track_import_settings: "Send track settings",
  gen_link: "Create link",
  connect_shop: "Connect Shop",
  integrated_shops_yet: "Formerly Integrated Shops",
  sales_from_not_connected_title: "Sales from not connected stores.",
  sales_from_not_connected_desc:
    "Stores that have had sales but are not yet integrated with Seller-Online.",

  track_import_settings_options: {
    no_send: {
      title: "Don't Send",
      desc: "We will not send tracking numbers to Etsy. You will need to enter them manually",
    },
    only_destination: {
      title: "Submit track",
      desc: "The track is sent along which the package is delivered to the buyer",
    },
    destination_after_us: {
      title: "Submit Track After Processing to USA",
      desc:
        "The track is sent to Etsy after the package has been received and processed at the US warehouse",
    },
  },

  other: {
    no_requests: "You haven't created any requests yet",
    description: `Setting up payment gateway from various online stores requires adding rights for your
        store in our PayPal account. To connect, please fill in all the required fields and
        create a request. Your request will be processed within one business day. After processing the request
        you will receive a notification to your email.`,
  }
};

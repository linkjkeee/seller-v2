export const ru = {
  login_placeholder: "Логин пользователя системы Seller Online",
  messages: {
    system_warning: "Перевод может осуществляться только между счетами пользователей Seller-Online.",
  },
};
export const ua = {
  login_placeholder: "Логін користувача системи Seller Online",
  messages: {
    system_warning: "Переказ може здійснюватись лише між рахунками користувачів Seller-Online.",
  },
};
export const en = {
  login_placeholder: "Seller Online system user login",
  messages: {
    system_warning: "Transfers can only be made between Seller-Online user accounts.",
  },
};

export const ru = {
  tg_welcome_message:
    "В Telegram есть наш канал для консультаций, обменом опыта и обсуждения новостей. Добро пожаловать!",
  new_design_warning:
    "Дорогие пользователи, мы обновили личный кабинет Seller Online. Это обновление призвано ускорить и упростить использование фукнкций системы. При возникновении проблем с использованием нового личного кабинета - воспользуйтесь старой версией и, при возможности, сообщите об ошибке в службу поддержки.",
  old_versions: "Старая версия",
  last_news: "Последние новости",
  last_created_shipments: "Недавно созданные отправки",
  last_account_operations: "Операции по счёту",
  new_shipment: "новую отправку",
};
export const ua = {
  tg_welcome_message: "У Telegram є наш канал для консультацій, обміну досвідом та обговорення новин. Ласкаво просимо!",
  new_design_warning:
    "Дорогі користувачі, ми оновили особистий кабінет Seller Online. Це оновлення покликане прискорити та спростити використання функцій системи. При виникненні проблем із використанням нового особистого кабінету - скористайтеся старою версією та, при можливості, повідомте про помилку до служби підтримки.",
  old_versions: "Стара версія",
  last_news: "Останнi новини",
  last_created_shipments: "Нещодавно створені відправлення",
  last_account_operations: "Операції по рахунку",
  new_shipment: "нове відправлення",
};
export const en = {
  tg_welcome_message:
    "Telegram has our channel for consultations, exchange of experience and discussion of news. Welcome!",
  new_design_warning:
    "Dear users, we have updated the Seller Online personal account. This update is designed to speed up and simplify the use of system functions. If you encounter problems using the new personal account, use the old version and, if possible, report the error to the support service.",
  old_versions: "Old version",
  last_news: "Latest news",
  last_created_shipments: "Recently Created Shipments",
  last_account_operations: "Account Operations",
  new_shipment: "new shipment",
};

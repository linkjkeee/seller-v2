export const ru = {
  sol_order_number: "Номер заказа Seller-Online",
  markets_order_number: "Номер заказа Etsy/Amazon/Shopify/eBay",
  comment_placeholder: "Пожелания к оформлению посылки",
  invoices_help: "к загрузке допускаются изображения и документы в формате PDF",
  customs_parcel_description_alert:
    "Проверьте, имеют ли Ваши товары корректное описание для таможни. Мы не несем ответственности за задержки при таможенном оформлении из-за неправильного описания товара.",

  to_office: "Доставка в наш офис напрямую",
  to_post_office: "Доставка через почтовое отделение",

  to_office_desc: "Курьер привезёт Вашу посылку в наш офис напрямую",
  to_post_office_desc: "Мы заберём Вашу посылку из отделения",

  create: {
    create_new_shipment: "Создать новую отправку",
    create_new_return: "Создать возвратную отправку",
  },

  returns: {
    action_deadline: "выберите действие до",
    return_action_confirm: `Действительно хотите выбрать опцию {action_title} для данной посылки?`,
    restock: "зачисление на склад",
    disposal: "утилизация",
    distribute_items: "распределение товаров",

    messages: {
      create_distribution_request_confirm: `Действительно хотите создать запрос на распределение для данной посылки?`,
      products_amount_error:
        "Количество распределенных товаров должно равняться общему количеству товаров",
      create_distribution_request_success: "Запрос на распределение успешно создан",
      distribution_request_created: "Запрос на распределение создан",
    },
  },

  small_pack: "мелкий пакет",
  avia: "авиа",
  ground_combined: "наземная комбинированная",
  gift: "подарок",
  sales_of_good: "продажа товаров",
  commercial_sample: "коммерческий образец",
  courier_send: "доставить курьером",
  big_parcel: "громоздкая посылка",
  deliver_notification: "уведомление о вручении",
  express_delivery: "экспресс-доставка",
  created_from_return: "Создано на основе возврата",

  type_desc: {
    usa_domestic: "Заявка на отправку товара, находящегося на хранении на складе в США.",
    svh: `Заявка на отправку посылок для хранения на склад в США. В дальнейшем после продажи товара отправка происходит непосредственно со склада, что позволяет существенно сократить сроки доставки.`,
    direct_desc: `Заявка на отправку посылки из Украины. Заполните адрес получателя и выберите тип отправки "Экспресс" или "Консолидация" при создании посылки.`,
  },

  errors: {
    product_create_not_allowed: "Вы не можете создать новые товары на складе",
  },

  messages: {
    stickers_warning: `Товары, не промаркированные с помощью складских стикеров, будут промаркированы нашими операторами по текущим тарифам.  Подробнее об услуге и о тарифах вы можете <a
      class="text-white"
      target="_blank"
      href="https://seller-online.com/novini-kompani%D1%97/zmini-u-tarifax-skladu/"
    >
      узнать в нашем блоге.
    </a>`,
    delivery_type_info:
      "Выберите способ доставки Вашей посылки в наш офис, от этого будет зависеть стоимость доставки",
    no_delivery_pay: "Вам не нужно оплачивать стоимость доставки в отделении.",
    pick_delivery_type: "Выберите способ доставки посылки до нашего офиса",
    type_not_picked:
      "Вы не выбрали способ доставки посылки до нашего офиса. Трек-номер не будет присвоен выбранным посылкам. Всё равно продолжить ?",

    //
    check_order_number: "Проверьте правильность номера заказа",
    check_invoices: "Необходимо загрузить подтверждающие документы",
    check_parcel_inner: "Добавьте товары в заявку",
    check_dimesions: "Проверьте правильность заполнения габаритов посылки",
    prompt_difference: `
      Таможенная стоимость товаров в посылке ({customs_cost}) ниже суммы заказа ({order_sum}). Стоимость доставки - {delivery_rate}. Разница составляет - {diff}. Возможно, занижена таможенная стоимость, продолжить ?
    `,
    return_created: "Возврат успешно создан",
    on_success_create_ttn: `После получения номера ТТН или номера отслеживания отправления, пожалуйста, внесите его в специальное поле в списке отправок. <br>`,
    orders_not_found: "Не найдено доступных заказов",
    category_products_was_added: `Все товары из выбранной категории добавлены в содержимое`,
    product_added: "Товар добавлен",
    address_loaded: "Адрес загружен",
    delivery_cost_is: "Стоимость отправки на склад в США составит:",

    //
    send_by_seller: `ОТПРАВКА ЧЕРЕЗ <b>Seller Online</b>`,
    send_by_seller_desc: "Отправить посылку используя наши ресурсы",
    sefl_send: `ОТПРАВКА <b>САМОСТОЯТЕЛЬНО</b>`,
    self_send_desc_html: `
    Вы самостоятельно отправляете посылку до нашего склада:
    <div class="q-mt-xs text-primary" style="font-weight:500">
      United States, 19053, Feasterville-Trevose, PA, 635 Somers ave, Seller Online, LLC, +1
      (267) 800-90-48, получатель: Warehouse Service
    </div>
    `,
    form_hemp: "Инструкция по заполнению формы",
    direct_dimensions_info:
      "Отправления ЭКСПРЕСС принимаются только в коробке, габариты которой минимум 15cm х 21cm. В противном случае стоимость упаковки будет списана со счета. Спасибо за понимание!",
    about_gift_html: `
        <p>
            Специальный раздел для оформления посылок <b>"Открытка"</b> - предназначен для заказов физического
            товара, по которым клиент оставил комментарий к заказу "не отсылать".
        </p>
        <p>Как это работает:</p>
        <ul>
            <li>
            Вы оформляете заявку через меню "Открытка", <b>указываете номер заказа</b> в системе
            Seller-Online, загружаете скриншот комментария к заказу или переписки с покупателем
            </li>
            <li>Оператор проверяет заявку</li>
            <li>
            По адресу покупателя со склада в США отправляется физическая открытка с текстом "Спасибо за
            помощь украинским продавцам", трек-номером открытки закрывается продажа
            </li>
            <li>Оплата за товар разблокируется на вашем счету</li>
        </ul>
      `,
    gift_location_warning: `В данный момент тип отправки "Открытка" работает <b>только для заказов из США </b> (за исключением Гавайев и Пуэрто-Рико)`,
    want_to_remove_product: "Действительно хотите удалить данный товар ?",
    product_created: "Товар создан",
    move_history: "История движения товара",
    tags_help:
      "С помощью ярлыка Вы можете отмечать отправки особыми свойствами, которые помогут Вам в будущем фильтровать заявки в списке созданных отправок",
    invoices_title: "Документы, подтверждающие стоимость",
    invoices_help:
      "Необходимо подтверждение стоимости товара для <b>отправки экспрессом</b> данного груза. Выписка о транзакции с paypal/etsy, ebay, amazon, shopify <b>на английском языке</b>",
    accept_chat_title: "Подтверждающая переписка",
    accept_chat_help:
      "Пожалуйста, прикрепите скриншоты переписки с покупателем или другую информацию, подтверждающую, что он просит не отправлять ему товар.",
    invoices_req: "до завантаження допускаються зображення та документи у форматі PDF",
    success_go_shipments:
      "Посмотреть свои отправки Вы можете в разделе <a href='/shippings.php'>“Посмотреть все отправления”</a><br /><br />",
    success_print_stickers: `
        <p>
            <b>Распечатайте стикеры</b> и закрепите их на Ваши товары, подготовленные к отправке на склад.
            <em>В случае, если стикеры с нашей нумерацией будут отсутствовать это увеличит срок принятия посылки на склад на 3-7 дней</em>
        </p>
    `,
    success_create_ttn: `
        После получения номера ТТН или номера отслеживания отправления, пожалуйста, внесите его в специальное поле в списке отправок.
    `,
    success_created_shipment: "Отправка успешно создана",
    go_to_shipments: "К списку отправок",
    create_ttn: "Создать ТТН Новой Почты",
    shipment_not_created: "Не удалось создать отправку",
    double_order_error: "По данному заказу уже создавалась отправка",
    customs_cost_warning:
      "Обратите внимание, что в некоторых странах низкий порог стоимости товаров и покупателю будет необходимо оплачивать таможенные пошлины.",
    inner_products_title: "Товары, выбранные для отправки",
    add_product_help:
      "Чтобы добавить товар в посылку - два раза кликните по нему в списке товаров.",
    invoices_count_warning: "Количество прикреплённых файлов не может быть больше 5",
    invoice_format_warning:
      "Один или несколько из загруженных файлов имеют недопустимый формат. К загрузке разрешены только изображения или PDF-документ",
    documents_created: "документы созданы",
    create_sender: "Создать новую запись отправителя",
    pick_sender: "Выберите отправителя",
    fields_must_be_latin_only: "Все поля должны быть заполнены латиницей",
    new_sender_record: "Новая запись отправителя",
    fill_recipient_data: "Заполните данные о получателе",
    pick_transort_type: "Выберите тип транспорта",
    pick_category: "Выберите категорию",
    input_parcel_inner: "Внесите информацию о содержимом посылки",
    input_parcel_dimensions: "Внесите информацию о габаритах",
  },

  list: {
    title_warning: `
      Для получения документов требуются названия товаров на
      <b>английском</b> и <b>украинском</b> языках. Перед отправкой убедитесь, что поля названий заполнены,
      иначе создать документ будет невозможно.
    `,
  },
};
export const ua = {
  sol_order_number: "Номер замовлення Seller-Online",
  markets_order_number: "Номер замовлення Etsy/Amazon/Shopify/eBay",
  comment_placeholder: "Побажання до оформлення посилки",
  invoices_help: "до завантаження допускаються зображення та документи у форматі PDF",
  customs_parcel_description_alert:
    "Перевірте, чи мають Ваші товари чіткий та правильний опис для митниці. Ми не несемо відповідальності за затримки при митному оформленні через неправильний опис товару.",

  to_office: "Доставка в наш офіс безпосередньо",
  to_post_office: "Доставка через поштове відділення",

  to_office_desc: "Кур'єр привезе Вашу посилку в наш офіс безпосередньо",
  to_post_office_desc: "Ми заберемо Вашу посилку з відділення",

  create: {
    create_new_shipment: "Створити нове відправлення",
    create_new_return: "Створити зворотне відправлення",
  },

  returns: {
    action_deadline: "оберіть дію до",
    return_action_confirm: `Дійсно хочете вибрати опцію {action_title} для даної посилки?`,
    restock: "зарахування на склад",
    disposal: "утилізація",
    restocked: "зараховано на склад",
    disposed: "утилізовано",

    distribute_items: "розподіл товарів",

    messages: {
      create_distribution_request_confirm: `Дійсно хочете створити запит на розподіл товарів ?`,
      products_amount_error:
        "Кількість розподілених товарів повинна дорівнювати загальній кількості товарів",
      create_distribution_request_success: "Запит на розподіл товарів успішно створений",
      distribution_request_created: "Запит на розподіл товарів створений",

      resend_action_success: "Повернення успішно надіслано",
      resend_action_confirm: `Дійсно хочете надіслати повернення ?`,
    },
  },

  small_pack: "дрібний пакет",
  avia: "авіа",
  ground_combined: "наземна комбінована",
  gift: "подарунок",
  sales_of_good: "продаж товарів",
  commercial_sample: "комерційний зразок",
  courier_send: "доставити кур'єром",
  big_parcel: "громіздка посилка",
  deliver_notification: "повідомлення про вручення",
  created_from_return: "Створено на основі повернення",
  express_delivery: "експрес-доставка",

  type_desc: {
    usa_domestic:
      "Заявка на відправку товару, що знаходиться на зберіганні на складі США.",
    svh: `Заявка на надсилання посилок для зберігання складу в США. Надалі після продажу товару відправлення відбувається безпосередньо зі складу, що дозволяє суттєво скоротити терміни доставки.`,
    direct_desc: `Заявка на надсилання посилки з України. Заповніть адресу одержувача та Оберіть тип надсилання "Експрес" або "Консолідація" під час створення посилки.`,
  },

  errors: {
    product_create_not_allowed: "Ви не можете створити нові товари на складі",
  },

  messages: {
    stickers_warning: `Товари, які не промарковані за допомогою складських стікерів, будуть промарковані нашими операторами за поточними тарифами. Докладніше про послугу та тарифи ви можете <a
    class="text-white"
    target="_blank"
    href="https://seller-online.com/novini-kompani%D1%97/zmini-u-tarifax-skladu/"
  >
    дізнатися у нашому блозі.
  </a>`,
    delivery_type_info:
      "Оберіть спосіб доставки Вашої посилки до нашого офісу, від цього залежатиме вартість доставки",
    no_delivery_pay: "Вам не потрібно сплачувати вартість доставки у відділенні.",
    pick_delivery_type: "Оберіть спосіб доставки посилки до нашого офісу",
    type_not_picked:
      "Ви не вибрали спосіб доставки посилки до нашого офісу. Трек-номер не буде присвоєний вибраним посилкам. Все одно продовжити?",

    check_order_number: "Перевірте правильність номера замовлення",
    check_invoices: "Необхідно завантажити документи, що підтверджують",
    check_parcel_inner: "Додайте товари в заявку",
    check_dimesions: "Перевірте правильність заповнення габаритів посилки",
    prompt_difference: `
      Митна вартість товарів у посилці ({customs_cost}) нижче за суму замовлення ({order_sum}). Вартість доставки – {delivery_rate}. Різниця становить – {diff}. Можливо, занижено митну вартість, продовжити ?
    `,
    return_created: "Повернення успішно створене",
    on_success_create_ttn: `Після отримання номера ТТН або номера відстеження відправлення, будь ласка, внесіть його у спеціальне поле у ​​списку відправок. <br>`,
    orders_not_found: "Не знайдено доступних замовлень",
    category_products_was_added: `Всі товари з обраної категорії додані до вмісту`,
    product_added: "Товар доданий",
    address_loaded: "Адреса завантажена",
    delivery_cost_is: "Вартість відправки на склад у США становитиме:",

    pick_category: `Оберіть або створіть категорію, щоб додати новий товар`,
    send_by_seller: `ВІДПРАВЛЕННЯ ЧЕРЕЗ <b>Seller Online</b>`,
    send_by_seller_desc: "Надіслати посилку використовуючи наші ресурси",
    sefl_send: `ВІДПРАВЛЕННЯ <b>САМОСТІЙНО</b>`,
    self_send_desc_html: `
        Ви самостійно відправляєте посилку до нашого складу:
        <div class="q-mt-xs text-primary" style="font-weight:500">
            United States, 19053, Feasterville-Trevose, PA, 635 Somers ave, Seller Online, LLC, +1
            (267) 800-90-48, отримувач: Warehouse Service
        </div>
    `,
    form_hemp: "Інструкція із заповнення форми",
    direct_dimensions_info:
      "Відправлення ЕКСПРЕС приймаються лише у коробці, габарити якої мінімум 15cm х 21cm. В іншому випадку вартість упаковки буде списана з рахунку. Дякумєо за розуміння!",
    about_gift_html: `
          <p>
            Спеціальний розділ для оформлення посилок <b>"Листівка"</b> - призначений для фізичних замовлень
            товару, за якими клієнт залишив коментар до замовлення "не надсилати".
          </p>
          <p>Як це працює:</p>
          <ul>
              <li>
              Ви оформляєте заявку через меню "Листівка", <b>вказуєте номер замовлення</b> в системі
              Seller-Online, завантажуєте скріншот коментаря до замовлення або листування з покупцем
              </li>
              <li>Оператор перевіряє заявку</li>
              <li>
              За адресою покупця зі складу в США надсилається фізична листівка з текстом "Спасибі за
              допомога українським продавцям", трек-номером листівки закривається продаж
              </li>
              <li>Оплата за товар розблокується на вашому рахунку</li>
          </ul>
        `,
    gift_location_warning: `На даний момент тип відправки "Листівка" працює <b>тільки для замовлень із США</b> (за винятком Гаваїв та Пуерто-Ріко)`,
    want_to_remove_product: "Чи дійсно хочете видалити цей товар?",
    product_created: "Товар створено",
    move_history: "Історія руху товару",
    tags_help:
      "За допомогою ярлика Ви можете відзначати надсилання особливими властивостями, які допоможуть Вам у майбутньому фільтрувати заявки у списку створених відправок",
    invoices_title: "Документи, що підтверджують вартість",
    invoices_help:
      "Необхідне підтвердження вартості товару для відправки експресом даного вантажу. Виписка про транзакцію з paypal/etsy, ebay, amazon, shopify <b>англійською мовою</b>",
    accept_chat_title: "Підтверджуюче листування",
    accept_chat_help:
      "Будь ласка, прикріпіть скріншоти листування з покупцем або іншу інформацію, що підтверджує, що він просить не надсилати йому товар.",
    invoices_req: "до завантаження допускаються зображення та документи у форматі PDF",
    success_go_shipments:
      "Переглянути свої відправки Ви можете у розділі <a href='/shippings.php'>“Переглянути всі відправлення”</a><br /><br />",
    success_print_stickers: `
        <p>
            <b>Роздрукуйте стікери</b> та закріпіть їх на Ваші товари, підготовлені до відправки на склад.
            <em>У разі, якщо стікери з нашою нумерацією будуть відсутні, це збільшить термін прийняття посилки на склад на 3-7 днів</em>
        </p>
    `,
    success_create_ttn: `
        Після отримання номера ТТН або відстеження відправлення, будь ласка, внесіть його в спеціальне поле у ​​списку відправок.
    `,
    success_created_shipment: "Відправлення успішно створено",
    go_to_shipments: "До списку відправок",
    create_ttn: "Створити ТТН Нової Пошти",
    shipment_not_created: "Не вдалося створити надсилання",
    double_order_error: "За цим замовленням вже створювалося відправлення",
    customs_cost_warning:
      "Зверніть увагу, що в деяких країнах низький поріг вартості товарів та покупцю буде необхідно сплачувати мита.",
    inner_products_title: "Товари, вибрані для надсилання",
    add_product_help:
      "Щоб додати товар у посилку - двічі клацніть по ньому у списку товарів.",
    invoices_count_warning: "Кількість прикріплених файлів не може перевищувати 5",
    invoice_format_warning:
      "Один або кілька із завантажених файлів мають неприпустимий формат. До завантаження дозволено лише зображення або PDF-документ",
    documents_created: "документи створені",
    create_sender: "Створити новий запис відправника",
    pick_sender: "Оберіть відправника",
    fields_must_be_latin_only: "Усі поля мають бути заповнені латиницею",
    new_sender_record: "Новий запис відправника",
    fill_recipient_data: "Заповніть дані про одержувача",
  },
};
export const en = {
  sol_order_number: "Seller-Online order number",
  markets_order_number: "Etsy/Amazon/Shopify/eBay order number",
  comment_placeholder: "Wishes for the package",
  invoices_help: "images and documents in PDF format are allowed to upload",
  customs_parcel_description_alert:
    "Check if your products have clear and correct description for customs. We are not responsible for delays in customs clearance due to incorrect description of goods.",

  to_office: "Delivery to our office directly",
  to_post_office: "Delivery via post office",

  to_office_desc: "Courier brought your package to our office without delays",
  to_post_office_desc: "We'll pick up your parcel for delivery from post office",

  create: {
    create_new_shipment: "Create a new shipment",
    create_new_return: "Create Return Shipment",
  },

  returns: {
    action_deadline: "select action before",
    return_action_confirm: `Do you really want to choose the {action_title} option for this shipment?`,
    restock: "restock",
    disposal: "disposal",
    restocked: "restocked",
    disposed: "disposed",
    distribute_items: "distribute items",

    messages: {
      resend_action_success: "Return successfully sent",
      resend_action_confirm: `Do you really want to send a return?`,

      create_distribution_request_confirm: `Do you really want to create a distribution request for this shipment?`,
      products_amount_error:
        "The number of distributed items must be equal to the total number of items",
      create_distribution_request_success: "Distribution request successfully created",
      distribution_request_created: "Distribution request created",
    },
  },

  small_pack: "small pack",
  avia: "air",
  ground_combined: "ground combined",
  gift: "gift",
  sales_of_good: "selling goods",
  commercial_sample: "commercial sample",
  courier_send: "deliver by courier",
  big_parcel: "bulky parcel",
  deliver_notification: "delivery notification",
  express_delivery: "express delivery",
  created_from_return: "Created based on return",

  type_desc: {
    usa_domestic: "Request to ship an item held in a US warehouse.",
    svh: `Application for sending parcels for storage to a warehouse in the USA. In the future, after the sale of goods, dispatch occurs directly from the warehouse, which can significantly reduce delivery times.`,
    direct_desc: `Application for sending a parcel from Ukraine. Fill in the recipient's address and select the type of shipment "Express" or "Consolidation" when creating the package.`,
  },

  errors: {
    product_create_not_allowed: "You cannot create new products in stock",
  },

  messages: {
    stickers_warning: `Goods not marked with warehouse stickers will be marked by our operators at the current rates. You can learn more about the service and tariffs <a
    class="text white"
    target="_blank"
    href="https://seller-online.com/novini-kompani%D1%97/zmini-u-tarifax-skladu/"
  >
    find out in our blog.
  </a>`,
    delivery_type_info:
      "Select the method of delivery of your package to our office, the cost of delivery will depend on this choice",
    no_delivery_pay: "You don't need to pay delivery charges at post office.",
    pick_delivery_type: "Choose a delivery method for the package to our office",
    type_not_picked:
      "You have not chosen the method of delivery of the parcel to our office. The track number will not be assigned to the selected parcels. Continue anyway?",

    check_order_number: "Check if the order number is correct",
    check_invoices: "You need to upload supporting documents",
    check_parcel_inner: "Add items to the ticket",
    check_dimesions: "Check that the package dimensions are filled in correctly",
    prompt_difference: `
      The customs value of the goods in the parcel ({customs_cost}) is lower than the amount of the order ({order_sum}). Shipping cost - {delivery_rate}. The difference is - {diff}. Perhaps the customs value is underestimated, continue?
    `,
    return_created: "Return successfully created",
    on_success_create_ttn: `After receiving the TTN number or tracking number of the shipment, please enter it in the special field in the shipment list. <br>`,
    orders_not_found: "No available orders found",
    category_products_was_added: `All products from the selected category have been added to the content`,
    product_added: "Product added",
    address_loaded: "Address loaded",
    delivery_cost_is: "The cost to ship to a US warehouse will be:",

    send_by_seller: `SENDING VIA <b>Seller Online</b>`,
    send_by_seller_desc: "Send a package using our resources",
    sefl_send: `SENDING <b>SELF</b>`,
    self_send_desc_html: `
    You yourself send the parcel to our warehouse:
    <div class="q-mt-xs text-primary" style="font-weight:500">
      United States, 19053, Feasterville-Trevose, PA, 635 Somers ave, Seller Online, LLC, +1
      (267) 800-90-48, recipient: Warehouse Service
    </div>
    `,
    form_hemp: "Form Instructions",
    direct_dimensions_info:
      "EXPRESS shipments are only accepted in a box that measures at least 15cm x 21cm. Otherwise, the cost of packaging will be deducted from the account. Thank you for your understanding!",
    about_gift_html: `
        <p>
            Специальный раздел для оформления посылок <b>"Открытка"</b> - предназначен для заказов физического
            товара, по которым клиент оставил комментарий к заказу "не отсылать".
        </p>
        <p>Как это работает:</p>
        <ul>
            <li>
            Вы оформляете заявку через меню "Открытка", <b>указываете номер заказа</b> в системе
            Seller-Online, загружаете скриншот комментария к заказу или переписки с покупателем
            </li>
            <li>Оператор проверяет заявку</li>
            <li>
            По адресу покупателя со склада в США отправляется физическая открытка с текстом "Спасибо за
            помощь украинским продавцам", трек-номером открытки закрывается продажа
            </li>
            <li>Оплата за товар разблокируется на вашем счету</li>
        </ul>
      `,
    gift_location_warning: `Currently, the 'Postcard' shipping type works <b>only for US orders</b> (excluding Hawaii and Puerto Rico)`,
    want_to_remove_product: "Are you sure you want to remove this product?",
    product_created: "Product created",
    move_history: "Product movement history",
    tags_help:
      "Using a label, you can mark shipments with special properties that will help you filter tickets in the list of created shipments in the future",
    invoices_title: "Documents confirming the cost",
    invoices_help:
      "Proof of value is required for <b>express</b> shipping of this shipment. Transaction statement from paypal/etsy, ebay, amazon, shopify <b>in English</b>",
    accept_chat_title: "Confirmation Chat",
    accept_chat_help:
      "Please attach screenshots of correspondence with the buyer or other information confirming that he asks not to ship the goods to him.",
    invoices_req: "images are allowed for submission in PDF format",
    success_go_shipments:
      "You can view your shipments in <a href='/shippings.php'>“View all shipments”</a><br /><br />",
    success_print_stickers: `
      <p>
          <b>Print stickers</b> and attach them to your goods ready to be sent to the warehouse.
          <em>If there are no stickers with our numbering, this will increase the period for accepting the parcel to the warehouse by 3-7 days</em>
      </p>
  `,
    success_create_ttn: `
      After receiving the TTN number or tracking number of the shipment, please enter it in a special field in the list of shipments.
  `,
    success_created_shipment: "Shipment created successfully",
    go_to_shipments: "Go to the list of shipments",
    create_ttn: "Create Nova Poshta track number",
    shipment_not_created: "Unable to create shipment",
    double_order_error: "A shipment has already been created for this order",
    customs_cost_warning:
      "Please note that some countries have a low threshold for the value of goods and the buyer will need to pay customs duties.",
    inner_products_title: "Selected products",
    add_product_help:
      "To add a product to your package, double click on it in the product list.",
    invoices_count_warning: "The number of attached files cannot exceed 5",
    invoice_format_warning:
      "One or more of the uploaded files is in an invalid format. Only images or a PDF document are allowed to upload",
    documents_created: "documents created",
    create_sender: "Create a new sender record",
    pick_sender: "Pick a sender",
    fields_must_be_latin_only: "All fields must be filled in latin",
    new_sender_record: "New sender record",
    fill_recipient_data: "Fill in recipient details",
    pick_transort_type: "Select Transport Type",
    pick_category: "Pick a category",
    input_parcel_inner: "Enter information about the contents of the parcel",
    input_parcel_dimensions: "Enter dimensions information",
  },

  list: {
    title_warning: `
      To obtain documents, the names of goods on
      <b>English</b> and <b>Ukrainian</b> languages. Before submitting, make sure that the title fields are filled in,
      otherwise it will be impossible to create the document.
    `,
  },
};

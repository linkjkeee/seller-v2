export const ru = {
  add_device: "Добавление нового устройства доступа",
  turn_on: "Включение двухфакторной аутентификации",
  otp_code: "Код из приложения OTP",
  scan: "просканируйте",
  on_new_device: "на новом устройстве",
  and_press_btn_below: "и нажмите кнопку ниже",
  new_device_added: "Новое устройство доступа успешно добавлено",
  input_code: "Введите код",
  "2fa_turned_on": "Двухфакторная аутентификация успешно активирована",
  help: `
        <div class="font-500">
          Что такое двухфакторная аутентификация
          </div>
          <div class="q-mt-md">
          Двухфакторная аутентификация — один из способов обезопасить вашу учетную запись.
          </div>
          <div class="q-mt-sm">
          Этот метод защиты данных сочетает в себе два элемента:
          <div class="q-ml-sm fz-13 flex items-center">
              -
              <div class="q-ml-xs">
              то, что вы знаете (логин и пароль);
              </div>
          </div>
          <div class="q-ml-sm fz-13 flex items-center">
              -
              <div class="q-ml-xs">
              то, что у вас есть (телефон, физический ключ безопасности, отпечаток пальца или другие биометрические
              данные).
              </div>
          </div>
          </div>
          <div class="q-mt-sm">
          Итого к обычному процессу входа в систему добавляется еще один этап: после ввода логина и пароля нужно ввести
          <b>код из SMS-сообщения</b>, <b>электронного письма</b> или <b>пуш-уведомления</b>.
          </div>
        `,
  "2fa_settings": "Настройки двухфакторной аутентификации",
  "2fa_warning":
    "Настоятельно рекомендуем включить двухфакторную аутентификацию для повышения безопасности Вашего аккаунта",
  account_protected: "Ваш аккаунт под надёжной защитой",
  init_new_device: "Добавить устройство входа",
  current_status: "Текущий статус",
  turn_off: "отключить двухфакторную аутентификацию",
};
export const ua = {
  add_device: "Додавання нового пристрою доступу",
  turn_on: "Включення двофакторної аутентифікації",
  otp_code: "Код із програми OTP",
  scan: "проскануйте",
  on_new_device: "на новому пристрої",
  and_press_btn_below: "та натисніть кнопку нижче",
  new_device_added: "Новий пристрій доступу успішно додано",
  input_code: "Введіть код",
  "2fa_turned_on": "Двофакторна аутентифікація успішно активована",
  help: `
      <div class="font-500">
      Що таке двофакторна аутентифікація
        </div>
        <div class="q-mt-md">
        Двофакторна аутентифікація — один із способів убезпечити ваш обліковий запис.
        </div>
        <div class="q-mt-sm">
        Цей метод захисту даних поєднує два елементи:
        <div class="q-ml-sm fz-13 flex items-center">
            -
            <div class="q-ml-xs">
            те, що ви знаєте (логін та пароль);
            </div>
        </div>
        <div class="q-ml-sm fz-13 flex items-center">
            -
            <div class="q-ml-xs">
            те, що у вас є (телефон, фізичний ключ безпеки, відбиток пальця або інші біометричні
              дані).
            </div>
        </div>
        </div>
        <div class="q-mt-sm">
        Таким чином, до звичайного процесу входу в систему додається ще один етап: після введення логіну та пароля потрібно ввести
        <b>код із SMS-повідомлення</b>, <b>електронного листа</b> або <b>пуш-повідомлення</b>.
        </div>
      `,
  "2fa_settings": "Налаштування двофакторної аутентифікації",
  "2fa_warning":
    "Рекомендуємо включити двофакторну аутентифікацію для підвищення безпеки Вашого облікового запису",
  account_protected: "Ваш обліковий запис під надійним захистом",
  init_new_device: "Додати пристрій входу",
  current_status: "Поточний статус",
  turn_off: "відключити двофакторну аутентифікацію",
};
export const en = {
  add_device: "Adding a new access device",
  turn_on: "Turn on two-factor authentication",
  otp_code: "Code from OTP app",
  scan: "scan",
  on_new_device: "on new device",
  and_press_btn_below: "and press the button below",
  new_device_added: "New access device added successfully",
  input_code: "Enter Code",
  "2fa_turned_on": "Two-factor authentication turned on successfully",
  help: `
    <div class="font-500">
      What is two-factor authentication
      </div>
      <div class="q-mt-md">
      Two-factor authentication is one way to secure your account.
      </div>
      <div class="q-mt-sm">
      This data protection method combines two elements:
      <div class="q-ml-sm fz-13 flex items-center">
          -
          <div class="q-ml-xs">
          what you know (login and password);
          </div>
      </div>
      <div class="q-ml-sm fz-13 flex items-center">
          -
          <div class="q-ml-xs">
          what you have (phone, physical security key, fingerprint or other biometric
          data).
          </div>
      </div>
      </div>
      <div class="q-mt-sm">
      In total, one more step is added to the normal login process: after entering the login and password, you need to enter
      <b>code from SMS message</b>, <b>email</b> or <b>push notification</b>.
      </div>
    `,
  "2fa_settings": "Two-factor authentication settings",
  "2fa_warning":
    "We strongly recommend that you enable two-factor authentication to increase the security of your account",
  account_protected: "Your account is secure",
  init_new_device: "Add an init device",
  current_status: "Current status",
  turn_off: "turn off two-factor authentication",
};

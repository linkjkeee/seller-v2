export const ru = {
  sales: {
    settings_data: "Данные для настройки",
    help_for: "Справка по",
    pp_settings: {
      title: "Настройка продаж на площадке Etsy",
      description:
        "Платежи, поступающие за продажи товаров на Etsy.com распознаются системой Seller-Online и зачисляются на счет клиента автоматически. Для этого достаточно указать наш email для приема Paypal платежей:",
      logins_title: "Укажите название Etsy магазина.",
      logins_desc: "Время зачисления платежа - до 15 минут.",
      no_address_warn: "В данный момент информация об адресе PayPal недоступна",
    },
    cms: {
      title: "Данные для настройки CMS, персональных интернет-магазинов",
      desc:
        "Для приема платежей через интернет-магазин понадобится дополнительная доработка его модуля оплаты через Paypal, которая заключается в необходимости передачи Вашим магазином на Paypal параметра CUSTOM, значение которого должно соответствовать Вашему персональному ключу, указанному ниже.",
      help_api: "Поручите эту задачу программисту, более подробную документацию он найдет на",
      help_note: "Если возникли вопросы - обращайтесь в службу поддержки.",
    },
    personal_links: {
      title: "Персональные ссылки для оплаты",
      info:
        "Просим обратить внимание, что управление персональными ссылками изменилось. Новые персональные ссылки так же позволяют принимать оплату кредитными картами, а не только через PayPal. Логика персональных ссылок тоже изменена. Есть одноразовые ссылки, которые работают сразу, есть многоразовые, для Landing страниц, которые работают после модерации.",
      manage: "Управление персональными ссылками",
    },
  },
};
export const ua = {
  sales: {
    settings_data: "Дані для налаштування",
    help_for: "Довідка по",
    pp_settings: {
      title: "Налаштування продажу на Etsy",
      description:
        "Платежі, що надходять за продаж товарів на Etsy.com, розпізнаються системою Seller-Online і зараховуються на рахунок клієнта автоматично. Для цього достатньо вказати наш email для прийому Paypal платежів:",
      logins_title: "Вкажіть назву Etsy магазину.",
      logins_desc: "Час зарахування платежу – до 15 хвилин.",
      no_address_warn: "В даний момент інформація про адресу PayPal недоступна",
    },
    cms: {
      title: "Дані для налаштування CMS, персональних інтернет-магазинів",
      desc:
        "Для прийому платежів через інтернет-магазин знадобиться додаткове доопрацювання його модуля оплати через Paypal, яке полягає в необхідності передачі Вашим магазином на Paypal параметра CUSTOM, значення якого має відповідати Вашому персональному ключу, вказаному нижче.",
      help_api: "Доручіть це завдання програмісту, докладнішу документацію він знайде на",
      help_note: "Якщо виникли запитання, зверніться до служби підтримки.",
    },
    personal_links: {
      title: "Персональні посилання для оплати",
      info:
        "Просимо звернути увагу на те, що управління персональними посиланнями змінилося. Нові персональні посилання також дозволяють приймати оплату кредитними картками, а не тільки через PayPal. Логіка персональних посилань також змінена. Є одноразові посилання, які працюють відразу, є багаторазові для Landing сторінок, які працюють після модерації.",
      manages: "Керування персональними посиланнями",
    },
  },
};
export const en = {
  sales: {
    settings_data: "Setting data",
    help_for: "Help on",
    pp_settings: {
      title: "Setting up sales on Etsy",
      description:
        "Payments received for the sale of goods on the Etsy.com auction are recognized by the Seller-Online system and credited to the client's account automatically. To do this, just enter our email to accept Paypal payments:",
      logins_title: "Enter your Etsy store name.",
      logins_desc: "Payment crediting time - up to 15 minutes.",
      no_address_warn: "PayPal address information is not available at this time",
    },
    cms: {
      title: "Data for setting up CMS, personal online stores",
      desc:
        "To accept payments through an online store, you will need to additionally refine its Paypal payment module, which consists in the need for your store to transfer the CUSTOM parameter to Paypal, the value of which must correspond to your personal key specified below.",
      help_api: "Give this task to a programmer, he will find more detailed documentation at",
      help_note: "If you have any questions, please contact support.",
    },
    personal_links: {
      title: "Personal Payment Links",
      info:
        "Please note that the management of personal links has changed. New personal links also allow you to accept payments by credit cards, and not only through PayPal. The logic of personal links has also been changed. There are one-time links that work immediately, there are reusable links for Landing pages that work after moderation.",
      manage: "Manage personal links",
    },
  },
};

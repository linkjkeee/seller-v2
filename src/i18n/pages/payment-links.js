export const ru = {
  payment_link: "ссылка для оплаты | ссылки для оплаты",
  pl_list: "список персональных ссылок для оплаты",
  pay_available: "возможность оплаты",
  payment_links_are_currently_disabled: "Ссылки для оплаты в данный момент отключены. Приносим свои извинения за доставленные неудобства.",

  statuses: {
    paid: "Оплачена",
    one_time: "Разовая",
    permanent: "Постоянная",
    in_review: "В рассмотрении",
    accepted: "Одобрена",
    declined: "Отклонена",
    blocked: "Заблокирована",
  },
};
export const ua = {
  payment_link: "посилання на оплату | посилання на оплату",
  pl_list: "список персональних посилань для оплати",
  pay_available: "можливість оплати",
  payment_links_are_currently_disabled: "Посилання для оплати на даний момент відключені. Просимо вибачення за завдані незручності.",

  statuses: {
    paid: "Оплачена",
    one_time: "Разова",
    permanent: "Постійна",
    in_review: "У розгляді",
    accepted: "Схвалено",
    declined: "Відхилена",
    blocked: "Заблокована",
  },
};
export const en = {
  payment_link: "payment link | payment links",
  pl_list: "personal payment links list",
  pay_available: "payable",
  payment_links_are_currently_disabled: "Payment links are currently disabled. We apologize for the inconvenience.",

  statuses: {
    paid: "Paid",
    one_time: "One use",
    permanent: "Permanent",
    in_review: "In review",
    accepted: "Approved",
    declined: "Declined",
    blocked: "Blocked",
  },
};

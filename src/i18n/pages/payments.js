export const ru = { payments_list: "Список платежей", target: "назначение", available: "доступен", waiting: "ожидает" };
export const ua = { payments_list: "Список платежів", target: "призначення", available: "доступний", waiting: "чекає" };
export const en = {
  payments_list: "List of payments",
  target: "destination",
  available: "available",
  waiting: "waiting",
};

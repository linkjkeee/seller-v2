export const ru = {
  info_message:
    "Нам поступили посылки без номеров, если Вы нашли посылку похожую на Вашу, просим прислать строчку с посылкой из этого списка и номер заказа, которому она соответствует, на",
  help_message:
    "Данные с нераспознанными заказами приходят в таком порядке: порядковый номер на складе, дата прихода на склад, отправитель (штат), номер лота или последние 5 цифр трэк номера, наименование вложения, вес в фунтах, состояние, тариф за прием на СВХ",
};
export const ua = {
  info_message:
    "Нам надійшли посилки без номерів, якщо Ви знайшли посилку схожу на Вашу, просимо надіслати рядок з посилкою з цього списку та номер замовлення, якому вона відповідає, на",
  help_message:
    "Дані з нерозпізнаними замовленнями приходять в такому порядку: порядковий номер на складі, дата приходу на склад, відправник (штат), номер лота або останні 5 цифр трек-номеру, найменування товару, вага у фунтах, статус, тариф СВХ",
};
export const en = {
  info_message:
    "We received parcels without numbers, if you find a parcel similar to yours, please send a line with a parcel from this list and the order number to which it corresponds to",
  help_message:
    "Data with unrecognized orders comes in the following order: serial number in the warehouse, date of arrival at the warehouse, sender (state), lot number or the last 5 digits of the track number, attachment name, weight in pounds, condition, tariff for acceptance to temporary storage warehouse",
};

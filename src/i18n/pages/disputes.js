export const ru = {
  active_disputes: "активные диспуты",
  label: "метка | метки",
  answer_to: "ответить до",
  //
  //
  //
  //
  question_resolved: "Вопрос решён",
  from_buyer: "от покупателя",
  from_operator: "от оператора",
  response_due: "ответить до",
  open_messages: "Открыть сообщения",
  accept_claim_chargeback: "Возврат средств",
  send_message: "Отправить сообщение",
  provide_tracking_info: "Предоставить информацию о доставке",
  accept_claim: "accept_claim",
  accept_claim_with_return: "Возврат товара",
  make_offer_refund_with_return: "Возврат средств после возврата товара",
  make_offer_different_amount: "Предложить другую сумму компенсации",

  under_moderation: "ожидает модерации",
  codes: {
    accepted: "принят",
    canceled_by_buyer: "отменен покупателем",
    denied: "отклонен",
    none: "отсутвует",
    resolved_buyer_favour: "решен в пользу покупателя",
    resolved_seller_favour: "решен в пользу продавца",
    resolved_with_payout: "решен с выплатой",
  },

  phase_warning: "В данной фазе невозможно отправить сообщение покупателю",
  reason_warning: "Причина диспута не позволяет отправить сообщение покупателю",
};
export const ua = {
  active_disputes: "активні диспути",
  label: "мітка | мітки",
  answer_to: "відповісти до",
  //
  //
  //
  //
  question_resolved: "Питання вирішено",
  from_buyer: "від покупця",
  from_operator: "від оператора",
  response_due: "відповісти до",
  open_messages: "Відкрити повідомлення",
  accept_claim_chargeback: "Повернення коштів",
  send_message: "Надіслати повідомлення",
  provide_tracking_info: "Надати інформацію про доставку",
  accept_claim: "accept_claim",
  accept_claim_with_return: "Повернення товару",
  make_offer_refund_with_return: "Повернення коштів після повернення товару",
  make_offer_different_amount: "Запропонувати іншу суму компенсації",

  under_moderation: "чекає модерації",
  codes: {
    accepted: "прийнято",
    canceled_by_buyer: "скасований покупцем",
    denied: "відхилений",
    none: "відсутня",
    resolved_buyer_favour: "вирішено на користь покупця",
    resolved_seller_favour: "вирішено на користь продавця",
    resolved_with_payout: "вирішено з виплатою",
  },
};
export const en = {
  active_disputes: "active disputes",
  label: "label | labels",
  answer_to: "answer before",
  //
  //
  //
  //
  question_resolved: "Issue resolved",
  from_buyer: "from buyer",
  from_operator: "from operator",
  response_due: "reply before",
  open_messages: "Open messages",
  accept_claim_chargeback: "Charge back",
  send_message: "Send a message",
  provide_tracking_info: "Send shipping info",
  accept_claim: "accept_claim",
  accept_claim_with_return: "Return Item",
  make_offer_refund_with_return: "Return cost after return item",
  make_offer_different_amount: "Request a different amount of compensation",

  under_moderation: "awaiting moderation",
  codes: {
    accepted: "accepted",
    canceled_by_buyer: "cancelled by buyer",
    denied: "rejected",
    none: "none",
    resolved_buyer_favor: "resolved in favor of buyer",
    resolved_seller_favor: "resolved in favor of seller",
    resolved_with_payout: "resolved with payout",
  },
};

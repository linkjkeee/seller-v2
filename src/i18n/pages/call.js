export const ru = {
  sol_call: "Позвонить в Seller-Online",
  sol_connection: "Соединение с Seller Online...",
  call: "позвонить",
  terminate: "завершить",
  call_terminated: "Звонок завершен",
  call_session: "звонок",

  deps: {
    support: "Клиентская поддержка",
    delivery: "Отдел доставки",
  },
};
export const ua = {
  sol_call: "Зателефонувати до Seller-Online",
  sol_connection: "З'єднання з Seller Online...",
  call: "зателефонувати",
  terminate: "завершити",
  call_terminated: "Дзвінок завершено",
  call_session: "дзвінок",

  deps: {
    support: "Клієнтська підтримка",
    delivery: "Відділ доставки",
  },
};
export const en = {
  sol_call: "Call Seller-Online",
  sol_connection: "Connecting to Seller Online...",
  call: "call",
  terminate: "terminate",
  call_terminated: "Call terminated",
  call_session: "call",

  deps: {
    support: "Customer support",
    delivery: "Delivery department",
  },
};

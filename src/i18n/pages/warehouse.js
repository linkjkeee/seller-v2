export const ru = {
  ua_description: "Описание товара для таможни на украинском языке",
  us_description: "Описание товара для таможни на английском языке",
  customs_description_alert: "Укажите четкое и правильное описание товара для таможни. Мы не несем ответственности за задержки при таможенном оформлении из-за неправильного описания товара.",
  mark_as_handmade: "Отметить как хендмейд",

  create_nested_category: "Создать дочернюю категорию",
  create_category_product: "Создать товар в категории",
  add_all_category_products: "Добавить все товары категории в посылку",
};
export const en = {
  ua_description: "Description of goods for customs in Ukrainian",
  us_description: "Item description for customs in English",
  customs_description_alert: "Provide clear and correct description of goods for customs. We are not responsible for delays in customs clearance due to incorrect description of goods.",
  mark_as_handmade: "Mark as handmade",

  create_nested_category: "Create a child category",
  create_category_product: "Create a product in a category",
  add_all_category_products: "Додати всі товари категорії в посилку",
};
export const ua = {
  ua_description: "Опис товару для митниці українською мовою",
  us_description: "Опис товару для митниці англійською мовою",
  customs_description_alert: "Вкажіть коректний опис товару для митниці. Ми не несемо відповідальності за затримки при митному оформленні через неправильний опис товару.",
  mark_as_handmade: "Позначити як хендмейд",

  create_nested_category: "Створити дочірню категорію",
  create_category_product: "Створити товар у категорії",
  add_all_category_products: "Add all category products in parcel",
};

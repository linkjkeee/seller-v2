export const ru = {
  messages: {
    fee_cards_desc: "Карты для оплати Fee",
    fee_cards_caption: "Здесь можно оформить заявку на виртуальную карту для оплаты Etsy fee.",
    help_message: "Помощь по работе с разделом",
  },

  list_title: "Запросы на виртуальные карты для оплаты",
  need_to_link_shop: "Требуется привязать магазин",

  help: {
    0: `После создания заявки, как только она будет подтверждена, мы переходим к добавлению карты на Etsy через AnyDesk. Обратитесь к нам в поддержку, чтобы произвести эту операцию (<a href="https://t.me/SOL_Tatyana" target="_blank">Telegram</a>).`,
    1: `Затем, Вам нужно будет пополнить карту в разделе «Мой счет» - «Вывод средств» - на виртуальную карту на сумму Etsy fee. После, можно оплачивать.`,
    2: `Если у Вас несколько магазинов и требуется несколько карт, то сделайте нужное количество заявок.`,
  },

  faq: {
    title: "Частозадаваемые вопросы",
    what_is_anydesk: "Что такое AnyDesk ?",
    anydesk_is:
      "Это программа удаленного доступа. Установите ее на ваш компьютер и впишите номер в форму. С помощью этой программы мы внесем созданную карту Вам на Etsy для оплат.",
    what_is_billing_address: "Что такое биллинг адрес?",
    billing_address_is: "Это ваш украинский адрес",
  },

  modal: {
    info:
      "При выпуске новой карты производится ее обязательное пополнение на 10 USD. Эта сумма будет списана с вашего счета. Минимальная сумма для пополнения карты - 10 USD. Обновление баланса карт производится один раз в сутки.",
    your_urk_address: "ваш украинский адрес",
  },
};
export const ua = {
  messages: {
    fee_cards_desc: "Картки для оплати Fee",
    fee_cards_caption: "Тут ви можете оформити заявку на віртуальну картку для оплати Etsy fee.",
    help_message: "Допомога у роботі з розділом",
  },
  list_title: "Запити на віртуальні карти.",
  need_to_link_shop: "Потрібно прив'язати магазин",

  help: {
    0: `Після створення заявки, як тільки вона буде підтверджена, ми переходимо до додавання картки на Etsy через AnyDesk. Зверніться до нас на підтримку, щоб зробити цю операцію (<a href="https://t.me/SOL_Tatyana" target="_blank">Telegram</a>).`,
    1: `Потім Вам потрібно буде поповнити картку в розділі «Мій рахунок» - «Виведення коштів» - на віртуальну картку на суму Etsy fee. Після цього можна оплачувати.`,
    2: `Якщо у Вас кілька магазинів і потрібно кілька карток, то зробіть потрібну кількість заявок.`,
  },

  faq: {
    title: "Часто задавані питання",
    what_is_anydesk: "Що таке AnyDesk?",
    anydesk_is:
      "Це програма віддаленого доступу. Встановіть її на ваш комп'ютер та впишіть номер у форму. За допомогою цієї програми ми внесемо створену картку на Etsy для оплат.",
    what_is_billing_address: "Що таке білінг адреса?",
    billing_address_is: "Це ваша українська адреса",
  },

  modal: {
    info:
      "При випуску нової картки проводиться обов'язкове її поповнення на 10 USD. Цю суму буде списано з вашого рахунку. Мінімальна сума для поповнення картки – 10 USD. Оновлення балансу карток проводиться один раз на добу.",
    your_urk_address: "ваша украинська адреса",
  },
};
export const en = {
  messages: {
    fee_cards_desc: "Fee Cards",
    fee_cards_caption: "Here you can apply for a virtual card to pay the Etsy fee.",
    help_message: "Help with section",
  },

  list_title: "Requests for virtual cards for payment",
  need_to_link_shop: "Store link required",

  help: {
    0: `After creating an application, once it is confirmed, we move on to adding a card to Etsy through AnyDesk. Contact our support to perform this operation (<a href="https://t.me/SOL_Tatyana" target="_blank">Telegram</a>).`,
    1: `Then, you will need to fund the card in the section "My Account" - "Withdraw Funds" - to a virtual card in the amount of Etsy fee. After, you can pay.`,
    2: `If you have several stores and require several cards, then make the required number of applications.`,
  },

  faq: {
    title: "FAQ",
    what_is_anydesk: "What is AnyDesk?",
    anydesk_is:
      "This is a remote access program. Install it on your computer and enter the number in the form. Using this program, we will transfer the created card to you on Etsy for payment.",
    what_is_billing_address: "What is the billing address?",
    billing_address_is: "This is your Ukrainian address",
  },

  modal: {
    info:
      "When a new card is issued, it must be topped up by 10 USD. This amount will be debited from your account. The minimum amount for card top-up is 10 USD. Card balance is updated once a day.",
    your_urk_address: "your Ukrainian address",
  },
};

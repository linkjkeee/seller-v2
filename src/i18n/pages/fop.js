export const ru = {
  title: "Регистрация ФЛП",
  current_requests: "Предыдущие заявки",

  register: {
    choose_product_type: "Выберите тип товара",
    physical_products: "Продажа физических товаров",
    digital_products: "Продажа цифровых товаров",
    as_in_international_passport: "как в загранпаспорте",
    address: "Адрес деятельности",
    phone: "Финансовый телефон",
    iban_currency: "IBAN валютного счёта",
    passport_files: "Копии страниц паспорта",
    inn_file: "Копия @:common.itn",
    registration_file: "Cвидетельство о регистрации",
    moved_person_file: "Справка переселенца",
    attached_files: "Прикрепленные файлы",
    send_signed_agreement: "Отправить подписанный договор",
    failed_to_get_credentials: "Не удалось получить информацию о договорах",
  },

  alerts: {
    max_files: "Вы можете загрузить не более 5 файлов",
    data_saved: "Данные сохранены",
    agreement_sent: "Подписанный договор отправлен",
    failed_to_send_agreement: "Не удалось отправить договор",
    confirm_delete: "Вы уверены, что хотите удалить эти данные?",
  },

  statuses: {
    invalid_data: "Неверные данные",
    wait_agreement: "Ожидает подписания",
    wait_approve: "Ожидает подтверждения",
    approved: "Подтверждён",
  },
};
export const ua = {
  title: "Реєстрація ФОП",
  current_requests: "Попередні заявки",

  register: {
    choose_product_type: "Оберіть тип товару",
    physical_products: "Продаж фізичних товарів",
    digital_products: "Продаж цифрових товарів",
    as_in_international_passport: "як у закордонному паспорті",
    address: "Адреса діяльності",
    phone: "Фінансовий телефон",
    iban_currency: "IBAN валютного рахунку",
    passport_files: "Копії сторінок паспорта",
    inn_file: "Копія @:common.itn",
    registration_file: "Cвідоцтво про реєстрацію",
    moved_person_file: "Довідка переселенця",
    attached_files: "Прикріплені файли",
    send_signed_agreement: "Відправити підписаний договір",
  },

  alerts: {
    max_files: "Ви можете завантажити не більше 5 файлів",
    data_saved: "Дані збережено",
    agreement_sent: "Підписаний договір відправлено",
    failed_to_send_agreement: "Не вдалося відправити договір",
    failed_to_get_credentials: "Не вдалося отримати інформацію про договори",
    confirm_delete: "Ви впевнені, що хочете видалити ці дані?",
  },

  statuses: {
    invalid_data: "Невірні дані",
    wait_agreement: "Очікує підписання",
    wait_approve: "Очікує підтвердження",
    approved: "Підтверджено",
  },
};
export const en = {
  title: "Registration of private entrepreneur",
  current_requests: "Previous requests",

  register: {
    choose_product_type: "Choose product type",
    physical_products: "Sale of physical goods",
    digital_products: "Sale of digital goods",
    as_in_international_passport: "as in international passport",
    address: "Business address",
    phone: "Financial phone",
    iban_currency: "IBAN of currency account",
    passport_files: "Copies of passport pages",
    inn_file: "Copy of @:common.itn",
    registration_file: "Certificate of registration",
    moved_person_file: "Certificate of resettlement",
    attached_files: "Attached files",
    send_signed_agreement: "Send signed agreement",
  },


  alerts: {
    max_files: "You can upload no more than 5 files",
    data_saved: "Data saved",
    agreement_sent: "Signed agreement sent",
    failed_to_send_agreement: "Failed to send agreement",
    failed_to_get_credentials: "Failed to get agreement information",
    confirm_delete: "Are you sure you want to delete this credentials?",
  },

  statuses: {
    invalid_data: "Invalid data",
    wait_agreement: "Waiting for signature",
    wait_approve: "Waiting for approval",
    approved: "Approved",
  },
};

export const ru = {
  title: "Затребование неопознанного платежа",
  customer_email: "Email покупателя",
  attach_info: "Предоставьте подтверждение в виде скриншота, при необходимости",
  send_request: "Отправить заявку",
};
export const ua = {
  title: "Вимога невідомого платежу",
  customer_email: "Email покупця",
  attach_info: "Надайте підтвердження у вигляді скріншота, за необхідності",
  send_request: "Надіслати заявку",
};
export const en = {
  title: "Unidentified payment request",
  customer_email: "Customer Email",
  attach_info: "Provide screenshot confirmation if needed",
  send_request: "Send request",
};

export const ru = {
  products_import: "импорт товаров",
  help: [
    "Убедитесь, что статус импорта товаров - ",
    "Загрузите шаблон документа",
    "Подготовьте документ со своими товарами в соответствии со загруженным шалоном",
    "Выберите подготовленный файл и нажмите",
  ],

  import_status: "статус импорта",
  ready_file: "подготовленный файл",
  start_import: "начать импорт",
  statuses: {
    failed: "",
    successful: "",
    canceled: "",
  },

  import_journal: "журнал импорта",
  available: "доступно",
  not_avsilable: "недоступно",
  new_import: "новый импорт",
}

export const en = {
  products_import: "import products",
  help: [
    "Make sure that products import status is ",
    "Download document template",
    "Prepare a document with your products according to the downloaded template",
    "Select prepared file and click",
  ],

  import_status: "import status",
  ready_file: "prepared file",
  start_import: "start import",
  statuses: {
    failed: "",
    successful: "",
    cancelled: "",
  },

  import_journal: "import journal",
  available: "available",
  not_avsilable: "not available",
  new_import: "new import",
};
export const ua = {
  products_import: "імпорт товарів",
  help: [
    "Переконайтеся, що статус імпорту товарів -",
    "Завантажте шаблон документа",
    "Підготуйте документ зі своїми товарами відповідно до завантаженого шалону",
    "Оберіть підготовлений файл і натисніть",
  ],

  import_status: "статус імпорту",
  ready_file: "підготовлений файл",
  start_import: "почати імпорт",
  statuses: {
    failed: "",
    successful: "",
    canceled: "",
  },

  import_journal: "журнал імпорту",
  available: "доступно",
  not_avsilable: "недоступно",
  new_import: "новий імпорт",
};

import { ru } from "./pages/2fa";
import { ru as agent_sales } from "./pages/agent-sales";
import { ru as cashback } from "./pages/cashback";
import { ru as dashboard } from "./pages/dashboard";
import { ru as disputes } from "./pages/disputes";
import { ru as fee_cards } from "./pages/fee-cards";
import { ru as get_money } from "./pages/get-money";
import { ru as lost_orders } from "./pages/lost-orders";
import { ru as new_buy } from "./pages/new-buy";
import { ru as pay_money } from "./pages/pay-money";
import { ru as payment_links } from "./pages/payment-links";
import { ru as payments } from "./pages/payments";
import { ru as send_money } from "./pages/send-money";
import { ru as settings } from "./pages/settings";
import { ru as shipments } from "./pages/shipments";
import { ru as integrations } from "./pages/integrations";
import { ru as lost_payments } from "./pages/lost-payments";
import { ru as import_products } from "./pages/import-products";
import { ru as call } from "./pages/call";
import { ru as services } from "./pages/services";
import { ru as crm } from "./pages/crm";
import { ru as warehouse } from "./pages/warehouse";

import { ru as common } from "./common/index.js";
import { ru as errors } from "./errors/index.js";
import { ru as layout } from "./layout/index.js";
import { ru as register } from "./pages/register";
import { ru as fop } from "./pages/fop";

export default {
  ...common,
  ...layout,
  errors,
  pages: {
    "2fa": ru,
    agent_sales,
    cashback,
    dashboard,
    disputes,
    fee_cards,
    get_money,
    lost_orders,
    new_buy,
    pay_money,
    payment_links,
    payments,
    send_money,
    settings,
    shipments,
    integrations,
    lost_payments,
    import_products,
    call,
    services,
    crm,
    register,
    warehouse,
    fop,
  },
};

import FormatStrings from "../utils/stringFormatter";
import { openURL, format, copyToClipboard } from "quasar";
const { capitalize } = format;

const EN_StringFormatter = new FormatStrings("en-US");
const RU_StringFormatter = new FormatStrings("ru-RU");

import DateFormatUtil from "src/utils/DateFormatter";

let { date, time } = new DateFormatUtil();

export default ({ Vue, ...app } /* { app, router, Vue ... } */) => {
  Vue.mixin({
    methods: {
      $toUSD(val, options = { currency: "USD" }) {
        return EN_StringFormatter.toCurrency(val, options);
      },
      $toFixed(val, fraction = 2) {
        return RU_StringFormatter.toLocaleFixed(val, [fraction, fraction]);
      },
      $parseApiError(e) {
        let message, errors;

        message = (e.response && e.response.data && e.response.data.message) || null;

        let ERROR =
          (e && e.response && e.response.data && e.response.data.errors) || null;

        if (ERROR) {
          if (Array.isArray(ERROR)) {
            errors = ERROR;
          } else {
            try {
              errors = Object.values(ERROR).flat();
            } catch (e) {
              console.log("On trying destruct 'object' error:", e);
            }
          }
        } else {
          errors = ERROR;
        }

        return {
          message,
          errors,
        };
      },
      $showApiError(
        e,
        {
          extended = false,
          fallbackMessage = null,
          level = "danger",
          notify = false,
        } = {}
      ) {
        let errors;

        const { message, errors: api_errors } = this.$parseApiError(e);

        if (extended) {
          errors = api_errors.map((error) => Object.values(error)[0] || null);
        } else {
          errors = api_errors;
        }

        this[notify ? "$message" : "$alert"]({
          level,
          errors,
          ...((!!message && {
            title: message,
            errors,
          }) || {
            message: e && e.toString(),
            title: fallbackMessage || "Ошибка сервера",
            errors,
          }),
        });
      },
      $capitalize(str) {
        return capitalize(str);
      },
      $openURL(params) {
        openURL(params);
      },
      $copyText(data) {
        return copyToClipboard(data);
      },
      $date(val) {
        return date(val);
      },
      $time(val) {
        return time(val);
      },
      $tt(code, capitalize = false, count = 1) {
        return capitalize
          ? this.$capitalize(this.$tc(code, count))
          : this.$tc(code, count);
      },
      $normalizeQuery({
        is_number = [],
        is_array = [],
        is_number_array = [],
        is_boolean = [],
      }) {
        return (query) =>
          Object.fromEntries(
            Object.entries(query)
              .filter(([k, v]) => {
                if (is_array.includes(k) || is_number_array.includes(k))
                  return v && !!v.length;
                else return v !== null && v !== undefined && v !== "";
              })
              .map(([k, v]) => {
                if (is_number.includes(k)) {
                  return [k, parseInt(v)];
                } else if (
                  // is_array.includes(k) &&
                  is_number_array.includes(k)
                ) {
                  let initial_is_array = Array.isArray(v);

                  if (initial_is_array) {
                    return [k, Array.from(v).map((el) => parseInt(el))];
                  } else {
                    return [k, [parseInt(v)]];
                  }
                } else if (is_boolean.includes(k)) {
                  return [k, typeof v === "string" ? (v === "true" ? true : false) : v];
                } else {
                  return [k, v];
                }
              })
          );
      },
      $formatCurrency(val, options = { currency: "USD" }) {
        return Intl.NumberFormat("en-US", {
          style: "currency",
          currency: options.currency,
        }).format(val);
      },
    },
  });

  Vue.filter("formatDate", (value) => {
    let DATE;

    try {
      DATE = new Date(value).toLocaleDateString();
    } catch (e) {
      console.log(e);
      return value;
    }

    return DATE;
  });
};

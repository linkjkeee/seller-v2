import Vue from "vue";
import axios from "axios";
import MANDATORY_MAP from "./mandatory.js";

const ACCESS_KEY = process.env.SELLER_API_ACCESS_KEY;
let API_URL;

if (process.env.DEV) {
  API_URL = process.env.LOCAL_API_URL;
  // API_URL = process.env.PRODUCTION_API_URL;

  console.log(
    `%cSOL ${process.env.NODE_ENV.toUpperCase()}`,
    "color:royalblue;font-family:system-ui;font-size:3rem;font-weight:bold"
  );
} else if (process.env.PROD) {
  API_URL = process.env.PRODUCTION_API_URL;
}

export const $api = axios.create({
  baseURL: API_URL,
  // timeout: 120000,
});

export default ({ Vue, router, redirect, store, app }) => {
  $api.interceptors.response.use(
    function(response) {
      return response;
    },
    function(error) {
      if (error && error.response && error.response.status === 403 && error.response.data) {
        let logout = error.response.data.logout || false;
        let mandatory = error.response.data.mandatory || false;

        if (logout) {
          store.dispatch("auth/AUTH_LOGOUT");

          router.push("/login");
        } else if (mandatory) {
          if (MANDATORY_MAP[mandatory]) {
            router.push(MANDATORY_MAP[mandatory]);

            return Promise.reject(error);
          } else {
            return Promise.reject(error);
          }
        } else {
          return Promise.reject(error);
        }
      } else {
        return Promise.reject(error);
      }
    }
  );

  window.$api = $api;
  Vue.prototype.$api = $api;
  // Vue.prototype.$axios = axios;
};

import { Notify } from "quasar";

Notify.setDefaults({
  position: "top",
  timeout: 3500,
  level: "default",
});

Notify.registerType("success", {
  icon: "check",
  color: "green-2",
  textColor: "green-8",
});
Notify.registerType("danger", {
  icon: "error",
  color: "red-2",
  textColor: "red-8",
});
Notify.registerType("warning", {
  icon: "warning",
  color: "yellow-2",
  textColor: "brown-5",
});
Notify.registerType("info", {
  icon: "info",
  color: "blue-2",
  textColor: "blue-8",
});
Notify.registerType("default", {
  color: "white",
  textColor: "black",
});

export default async ({ Vue } /* { app, router, Vue ... } */) => {
  Vue.prototype.$message = ({ level, title, icon = null, caption = null, ...quasarOptions }, timeout = 3500) => {
    Notify.create({
      message: title,
      caption,
      type: level,
      ...(icon && { icon }),
      ...quasarOptions,
      timeout,
      // actions: [
      //   {
      //     label: "OK",
      //     color: "white",
      //     handler: () => {
      //       /* ... */
      //     },
      //   },
      // ],
    });
  };
};

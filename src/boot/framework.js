import { Screen } from "quasar";

export default async (/* { app, router, Vue ... } */) => {
  // something to do

  Screen.setSizes({ xs: 480, sm: 599, md: 768, lg: 1023, xl: 1140 + 28 });
};

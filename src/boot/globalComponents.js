import VueModal from "vue-js-modal";
import WithLoading from "../components/_ui/WithLoading";
import PageContainer from "../components/_ui/PageContainer";
import DialogWrapper from "../components/_ui/DialogWrapper";
import WithTransition from "../components/_ui/WithTransition";
import CustomsCodes from "../components/CustomsCodes";
import AppBanner from "../components/_ui/Banner";
import AppList from "../components/_ui/List";
import AppBtn from "../components/_ui/Button";
import AppBtnDropdown from "../components/_ui/Button/dropdown.vue";
import AppTextInput from "../components/_ui/TextInput";
import AppStringCropper from "../components/_ui/StringCropper";
import AppDatePickerInput from "../components/_ui/DatePickerInput";
import AppFlagIcon from "../components/_ui/FlagIcon";
import AppImage from "../components/_ui/LazyImage";
import AppTableList from "../components/_ui/TableList";
import AppBadge from "../components/_ui/Badges";
import DeprecationWarning from "../components/DeprecationWarning";
import AppSelect from "../components/_ui/Select";
import LocalDatePicker from "../components/LocalDatePicker";
import Pagination from "../components/_ui/Pagination";
import BadgeSelector from "../components/BadgeSelector";

export default ({ Vue, router, app }) => {
  Vue.use(VueModal, {
    injectModalsContainer: true,
    dialog: true,
    dynamic: true,
  });

  // Vue.use(autoAnimatePlugin);
  Vue.component("customs-codes", CustomsCodes);
  // Vue.component("string-cropper", AppStringCropper);

  Vue.component("with-loading", WithLoading);
  Vue.component("with-transition", WithTransition);
  Vue.component("PageContainer", PageContainer);
  Vue.component("DialogWrapper", DialogWrapper);
  Vue.component("app-banner", AppBanner);
  Vue.component("app-list", AppList);
  Vue.component("app-btn", AppBtn);
  Vue.component("app-btn-dropdown", AppBtnDropdown);
  Vue.component("app-input", AppTextInput);
  Vue.component("app-select", AppSelect);
  Vue.component("date-picker", AppDatePickerInput);
  Vue.component("app-flag", AppFlagIcon);
  Vue.component("app-img", AppImage);
  Vue.component("app-table-list", AppTableList);
  Vue.component("app-badge", AppBadge);
  Vue.component("deprecation-warning", DeprecationWarning);
  Vue.component("LocalDatePicker", LocalDatePicker);
  Vue.component("app-pagination", Pagination);
  Vue.component("BadgeSelector", BadgeSelector);
};

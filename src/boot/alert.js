// import Banner from "../components/_ui/Alert";
import Store from "../store";
import { Dialog } from "quasar";
import Panel from "../components/_ui/Alert";

export default async ({ Vue } /* { app, router, Vue ... } */) => {
  Vue.mixin({
    methods: {
      $alert({
        title = null,
        message = null,
        level = "primary",
        width = 490,
        name = "_alert",
        actions = null,
        errors = null,
        closeText = this.$tt("common.close"),
        nextText = this.$tt("common.continue"),
        minWidth,
        ...dialog_props
      }) {
        return new Promise(function(done, fail) {
          Dialog.create({
            component: Panel,
            // * ALERT PROPS
            title,
            message,
            level,
            actions,
            errors,
            closeText,
            nextText,
            minWidth,
            ...dialog_props,
          })
            .onOk(() => {
              done();
            })
            .onCancel(() => {
              done();
            })
            .onDismiss(() => {
              done();
            });

          done();
        });
      },
      $confirm({
        actions,
        name = "_confirm",
        width = 560,
        is_confirm = true,
        ...content_options
      }) {
        return new Promise((ok, fail) => {
          Dialog.create({
            component: Panel,
            actions,
            is_confirm,
            persistent: true,
            ...content_options,
            parent: this,
          })
            .onOk((answ) => {
              ok(answ);
            })
            .onCancel(() => {
              fail(null);
            });
        });
      },
    },
  });
};

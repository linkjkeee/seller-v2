import { AddressbarColor } from "quasar";

export default () => {
  AddressbarColor.set("#00897b");
};

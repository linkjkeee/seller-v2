// import something here

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async ({ Vue } /* { app, router, Vue ... } */) => {
  // something to do

  Vue.directive("outside-click", {
    bind: function(e, b, node) {
      e.clickOutsideEvent = function(event) {
        if (!(e == event.target || e.contains(event.target))) {
          node.context[b.expression](event);
        }
      };
      document.body.addEventListener("click", e.clickOutsideEvent);
    },
    unbind: function(el) {
      document.body.removeEventListener("click", el.clickOutsideEvent);
    }
  });
};

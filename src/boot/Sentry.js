// Project Settings -> General Settings -> Allowed Domains
import * as Sentry from "@sentry/vue";
import { Integrations } from "@sentry/tracing";

const IS_DEV = process.env.DEV;

export default async ({ app, Vue }) => {
  if (!IS_DEV || process.env.SENTRY_ENABLED) {
    Sentry.init({
      Vue,
      dsn: "https://8a5e148672224f8682a9237c36808818@sentry.seller-online.com/9",
      integrations: [
        new Integrations.BrowserTracing({
          routingInstrumentation: Sentry.vueRouterInstrumentation(app.router),
        }),
      ],
      tracesSampleRate: 1.0,
      environment: process.env.NODE_ENV,
      autoSessionTracking: false, // ?????
    });
    Vue.config.logErrors = true;
    Vue.config.errorHandler = function(e = "Unknown error", vm, info) {
      console.log("@sentry catch error:", e, vm, info);

      Sentry.setContext("JS_INFO", {
        info,
        vm,
      });

      Sentry.captureException(new Error(e));
    };
  }
};

import { colors } from "quasar";
const { getPaletteColor } = colors;

const accents = [
  "red",
  "pink",
  "purple",
  "deep-purple",
  "indigo",
  "blue",
  "cyan",
  "teal",
  "green",
  "light-green",
  "lime",
  "yellow",
  "amber",
  "orange",
  "deep-orange",
  "brown",
  "grey",
  "blue-grey",
];

const getRandomIndex = (min = 0, max = accents.length - 1) => {
  let rand = min - 0.5 + Math.random() * (max - min + 1);
  return Math.round(rand);
};

export default {
  getRandomAccent: () => {
    let ridx = getRandomIndex();
    let colorStr = accents[ridx];

    return getPaletteColor(colorStr);
  },

  getRandomTone: (min = 1, max = 14) => {
    let color_idx = getRandomIndex(),
      tone_index = getRandomIndex(min, max);

    return getPaletteColor(`${accents[color_idx]}-${tone_index}`);
  },

  getUniqeRange(range) {
    let t = accents.slice(0, range);

    t = t.map((str) => getPaletteColor(str));

    return t;
  },

  // getPaletteColor,
};

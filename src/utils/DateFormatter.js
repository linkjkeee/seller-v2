import dayjs from "dayjs";
var customParseFormat = require("dayjs/plugin/customParseFormat");
dayjs.extend(customParseFormat);

import RuLocale from "dayjs/locale/ru";

export default class DateFormatter {
  constructor(options) {
    // let { mask = null } = options;
  }

  format(val) {
    return dayjs(val).format("DD.MM.YYYY");
  }

  date(val, format = "DD.MM.YYYY") {
    return dayjs(val).format(format);
  }
  time(val, format = "HH:mm") {
    return dayjs(val).format(format);
  }
}

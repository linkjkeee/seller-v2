export default class LocaleString {
  constructor(defaultRegion = navigator.language || undefined) {
    if (!defaultRegion) throw new Error("Region locale is UNDEFINED");
    this.region = defaultRegion;
  }

  normalizeZero() {
    return +"0";
  }

  /**
   *
   * @param value value to convert
   * @param options options for convert
   * @param region user's region @default navigator.language
   */

  toCurrency(
    value,
    options = {
      style: "currency",
      currency: "RUB"
    },
    region = this.region
  ) {
    const _options = { style: "currency", ...options };

    if (value === null) {
      return null;
    }

    if (!value) {
      return this.normalizeZero().toLocaleString(region, _options);
    }

    if (isNaN(value)) {
      throw new Error("Type ERROR: value is not a number");
    }

    return value.toLocaleString(region, _options);
  }

  toLocaleFixed(
    value,
    [minimumFractionDigits = 2, maximumFractionDigits = minimumFractionDigits]
  ) {
    if (!value) {
      return this.normalizeZero().toLocaleString(this.region, {
        minimumFractionDigits,
        maximumFractionDigits
      });
    }
    return value.toLocaleString(this.region, {
      minimumFractionDigits,
      maximumFractionDigits
    });
  }
}

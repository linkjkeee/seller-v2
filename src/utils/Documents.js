export default class DocumentsHelper {
  _namespace;
  _date;

  constructor(namespace = "document") {
    this._namespace = namespace;
    this._date = new Date()
      .toLocaleDateString()
      .split(".")
      .join("-");
  }

  get_blob(data) {
    return new Blob(data);
  }

  get_url(data) {
    return URL.createObjectURL(this.get_blob(data));
  }

  get_file_name(custom) {
    if (custom) {
      return custom + "_" + this._namespace + "-" + this._date;
    } else {
      return this._namespace + "-" + this._date;
    }
  }

  show() {}

  print() {}

  download(data, filename = this.get_file_name(filename)) {
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(this.get_blob(data), filename);
    } else {
      const link = document.createElement("a");
      if (link.download !== undefined) {
        let url = this.get_url(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = "hidden";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }
}

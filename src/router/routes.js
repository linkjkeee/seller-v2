const routes = [
  {
    path: "/",

    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        name: "HOME",
        path: "/",
        component: () => import("pages/Index.vue"),
        redirect: "/dashboard",
      },
      {
        name: "AUTH_LOGIN",
        path: "/login",
        component: () =>
          import(/* webpackChunkName: "auth-login" */ "pages/Auth/Login.vue"),
      },
      {
        name: "AUTH_REGISTER",
        path: "/register",
        component: () =>
          import(/* webpackChunkName: "auth-register" */ "pages/Auth/Registration.vue"),
      },
      {
        name: "AUTH_PASSWORD_LOST",
        path: "/restore-password",
        component: () =>
          import(/* webpackChunkName: "auth-restore" */ "pages/Auth/PasswordLost.vue"),
      },
      {
        name: "AUTH_LOST_REQUEST",
        // ?action=change&key=1642779730&sign=2ddfb59ed82702e6f36080099f019b43a817bbd2&login=selleronline5%40icloud.com
        path: "/login/forgotten",
        component: () =>
          import(/* webpackChunkName: "auth-restore" */ "pages/Auth/Request.vue"),
      },
      {
        name: "DASHBOARD",
        path: "/dashboard",
        component: () => import("pages/Dashboard"),
        meta: { requireAuth: true },
      },
      {
        name: "SHIPMENTS_LIST",
        path: "/shipments/list",
        component: () =>
          import(/* webpackChunkName: "shipments" */ "pages/Shipments/List"),
        props: { create_ttn: false },
        meta: {
          requireAuth: true,
          validate_account: true,
          section: "SHIPMENTS",
        },
      },
      {
        name: "PROCESS_SHIPMENTS",
        path: "/shipments/process",
        component: () =>
          import(/* webpackChunkName: "shipments" */ "pages/Shipments/Process/index.vue"),
      },
      {
        name: "CREATE_SHIPMENT",
        path: "/shipments/create",
        component: () =>
          import(/* webpackChunkName: "shipments" */ "pages/Shipments/Create"),
        meta: {
          requireAuth: true,
          validate_account: true,
          section: "SHIPMENTS",
        },
      },
      {
        name: "MY_ADDRESSES",
        path: "/addresses",
        component: () => import(/* webpackChunkName: "addresses" */ "pages/Addresses"),
        meta: { requireAuth: true },
      },
      {
        name: "NEW_ADDRESS",
        path: "/addresses/new",
        component: () =>
          import(/* webpackChunkName: "addresses" */ "pages/Addresses/NewAddress.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "EDTI_ADDRESS",
        path: `/addresses/:id/edit`,
        component: () =>
          import(/* webpackChunkName: "addresses" */ "pages/Addresses/NewAddress.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "ACCOUNT",
        path: "/account",
        component: () => import(/* webpackChunkName: "account" */ "pages/Account"),
        meta: { requireAuth: true },
      },
      {
        name: "ACCOUNT_EDIT",
        path: "/account/edit",
        component: () => import(/* webpackChunkName: "account" */ "pages/Account"),
        props: { edit: true },
        meta: { requireAuth: true },
      },
      {
        name: "CASHBACK",
        path: "/cashback",
        component: () => import("pages/Cashback"),
        meta: { requireAuth: true },
      },
      {
        name: "PRODUCTS",
        path: "/products",
        component: () => import("pages/Warehouse"),
        meta: { requireAuth: true },
      },
      {
        name: "WAREHOUSE_PRODUCTS",
        path: "/products/warehouse",
        component: () => import("pages/Warehouse"),
        meta: { requireAuth: true },
        props: { warehouse: true },
      },
      {
        name: "CONNECT_SHOP",
        path: "/connect/:platform",
        component: () => import("pages/Connect"),
        meta: { requireAuth: true, section: "INTEGRATIONS" },
      },
      {
        name: "MONEY_IN",
        path: "/money-in",
        component: () => import(/* webpackChunkName: "money-in" */ "pages/Money/In"),
        meta: { requireAuth: true, section: "INV" },
      },
      {
        name: "MONEY_IN_CHECKOUT",
        path: "/money-in/checkout",
        component: () =>
          import(/* webpackChunkName: "money-in" */ "pages/Money/In/Checkout"),
        meta: { requireAuth: true, section: "INV" },
      },
      {
        name: "INVOICES",
        path: "/invoices",
        component: () => import(/* webpackChunkName: "invoices" */ "pages/Invoices"),
        meta: { requireAuth: true, section: "INV" },
      },
      {
        name: "INVOICE_ITEM",
        path: "/invoices/:id",
        component: () =>
          import(/* webpackChunkName: "invoices" */ "pages/Invoices/Details"),
        meta: { requireAuth: true, section: "INV" },
      },
      {
        name: "RETURNS",
        path: "/shipments/returns",
        component: () => import(/* webpackChunkName: "returns" */ "pages/Returns"),
        meta: { requireAuth: true, section: "RETURNS" },
      },
      {
        name: "UNIDENTIFIED_RETURNS",
        path: "shipments/returns/unidentified",
        component: () =>
          import(
            /* webpackChunkName: "returns-unidentified" */ "pages/UnidentifiedReturns"
          ),
        meta: {
          requireAuth: true,
          section: "RETURNS",
        },
      },
      {
        name: "CLAIM_UNIDENTIFIED_RETURN",
        path: "shipments/returns/unidentified/:return_id/claim",
        component: () =>
          import(
            /* webpackChunkName: "returns-unidentified" */ "pages/UnidentifiedReturns/ReturnClaim.vue"
          ),
        meta: { requireAuth: true, section: "RETURNS" },
      },
      {
        name: "HS_CODES_CATALOG",
        path: "/hs-codes",
        component: () => import("pages/HsCodes"),
      },
      {
        name: "LEGAL_INFO",
        path: "/legal-info",
        component: () => import("pages/LegalInfo"),
      },
      {
        name: "LEGAL_INFO_DOCUMENT",
        path: "/legal-info/:document",
        component: () => import("pages/LegalInfo/document.vue"),
      },
      {
        name: "CALCULATOR",
        path: "/calculator",
        component: () => import("pages/Calculator"),
      },
      {
        name: "PAYMENTS_INCOME",
        path: "/payments/:type",
        component: () => import("pages/Payments/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "PAY_MONEY",
        path: "/pay-money",
        component: () =>
          import(/* webpackChunkName: "pay-money" */ "pages/PayMoney/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "PAY_CHECKOUT",
        path: "/pay-money/checkout",
        component: () =>
          import(/* webpackChunkName: "pay-money" */ "pages/PayMoney/Checkout/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "GET_MONEY",
        path: "/get-money",
        component: () =>
          import(/* webpackChunkName: "get-money" */ "pages/GetMoney/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "GET_MONEY_CHECKOUT",
        path: "/get-money/checkout",
        component: () =>
          import(/* webpackChunkName: "get-money" */ "pages/GetMoney/Checkout/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "GET_MONEY_REQUISITES",
        path: "/requisites/:method",
        component: () =>
          import(/* webpackChunkName: "get-money" */ "pages/Requisites/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "SEND_MONEY",
        path: "/send-money",
        component: () => import("pages/SendMoney/index.vue"),
        meta: { requireAuth: true, section: "PAYMENTS" },
      },
      {
        name: "PAYONEER_ACCOUNTS",
        path: "/payoneer-accounts",
        component: () =>
          import(
            /* webpackChunkName: "payoneer-accounts" */ "pages/PayoneerAccount/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "ACTIVATE_PAYONEER",
        path: "/payoneer-accounts/:account_id/activate/",
        component: () =>
          import(
            /* webpackChunkName: "payoneer-accounts" */ "pages/PayoneerAccount/activate.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "BANALCE_LOG",
        path: "/balance-log",
        component: () => import("pages/BalanceLog/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "MONEY_BLOCK_LOG",
        path: "/money-hold-log",
        component: () => import("pages/MoneyBlockLog/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "ORDERS",
        path: "/orders/:type",
        component: () =>
          import(/* webpackChunkName: "orders" */ "pages/Orders/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "ONE_ORDER",
        path: "/orders/:type/:order_id",
        component: () =>
          import(/* webpackChunkName: "orders" */ "pages/Orders/Sell/Order/index.vue"),
        meta: { requireAuth: true },
      },

      {
        name: "PAYMENT_LINK",
        path: "/payment-links",
        component: () =>
          import(/* webpackChunkName: "payment-links" */ "pages/PaymentLinks/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "CREATE_PAYMENT_LINK",
        path: "/payment-links/new",
        component: () =>
          import(
            /* webpackChunkName: "payment-links" */ "pages/PaymentLinks/CreateLink.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "LOST_PAYMENTS",
        path: "/lost-payments/",
        component: () => import("pages/LostPayments/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "NEW_BUY",
        path: "/buy/new",
        component: () => import("pages/Buy/New/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "BUY_CREATE_PARCEL",
        path: "/buy/create-parcel",
        component: () =>
          import(/* webpackChunkName: "buyments" */ "pages/Buy/CreateParcel/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "SEND_BUY",
        path: "/buy/send",
        component: () =>
          import(/* webpackChunkName: "buyments" */ "pages/Buy/Send/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "US_PARCELS",
        path: "buy/parcels",

        component: () =>
          import(/* webpackChunkName: "buyments" */ "pages/Buy/Parcels/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "LOST_ORDERS",
        path: "/lost-orders",

        component: () => import("pages/LostOrders/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "UKRPOSHTA_SHIPMENTS",
        path: "/ukrposhta-shipments",

        component: () =>
          import(
            /* webpackChunkName: "ukrposhta" */ "pages/UkrposhtaShipments/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "NEW_UKRPOSHTA_SHIPMENT",
        path: "/ukrposhta-shipments/new",
        component: () =>
          import(
            /* webpackChunkName: "ukrposhta" */ "pages/UkrposhtaShipments/New/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "SUBACCOUNTS",
        path: "/subaccounts",
        component: () =>
          import(/* webpackChunkName: "subaccounts" */ "pages/Subaccounts/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "NEW_SUBACCOUNT",
        path: "/subaccounts/new",
        component: () =>
          import(/* webpackChunkName: "subaccounts" */ "pages/Subaccounts/New/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "EDIT_SUBACCOUNT",
        path: "/subaccounts/edit",
        component: () =>
          import(/* webpackChunkName: "subaccounts" */ "pages/Subaccounts/New/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "FOP_REGISTER",
        path: "/fop-register",
        component: () => import("pages/Fops"),
        meta: { requireAuth: true },
      },
      {
        name: "SALES_SETTINGS",
        path: "/settings/sales",
        component: () =>
          import(/* webpackChunkName: "settings" */ "pages/Settings/Sales/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "ACCOUNT_PASSPORT_INFO",
        path: "/settings/account-passport-info",
        component: () =>
          import(
            /* webpackChunkName: "settings" */ "pages/Settings/PassportInfo/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "CHANGE_PASSWORD",
        path: "/settings/change-password",
        component: () =>
          import(
            /* webpackChunkName: "settings" */ "pages/Settings/ChangePassword/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "2FA_AUTH",
        path: "/settings/2fa",
        component: () =>
          import(/* webpackChunkName: "settings" */ "pages/Settings/2fa/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "2FA_INIT",
        path: "/settings/2fa/init",
        component: () =>
          import(/* webpackChunkName: "settings" */ "pages/Settings/2fa/Init/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "2FA_ADD",
        path: "/settings/2fa/add",
        component: () =>
          import(/* webpackChunkName: "settings" */ "pages/Settings/2fa/Init/index.vue"),
        meta: { requireAuth: true },
        props: { add: true },
      },
      {
        name: "SUMMARY_REPORT",
        path: "/account/summary-report",
        component: () => import("pages/SummaryReport"),
        meta: { requireAuth: true },
      },
      {
        name: "PAYPAL_DISPUTES",
        path: "/resolution-center",
        component: () =>
          import(
            /* webpackChunkName: "resolution-center" */ "pages/PaypalDisputes/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "PAYPAL_ONE_DISPUTE",
        path: "/resolution-center/dispute/:id",
        component: () =>
          import(
            /* webpackChunkName: "resolution-center" */ "pages/PaypalDisputes/Dispute/index.vue"
          ),
        meta: { requireAuth: true },
      },
      {
        name: "CART",
        path: "/cart",
        component: () => import(/* webpackChunkName: "cart" */ "pages/Cart/index.vue"),
      },
      // {
      //   name: "FEE_CARDS",
      //   path: "/fee-cards",
      //   component: () => import(`pages/FeeCards/index.vue`),
      //   meta: { requireAuth: true },
      // },
      {
        name: "SEND_FEEDBACK",
        path: "/send-feedback",
        component: () => import(`pages/SendFeedback/index.vue`),
        meta: { requireAuth: true },
      },
      {
        name: "NOTIFICATIONS_CENTER",
        path: "/notifications",
        component: () => import("pages/Notifications"),
        meta: { requireAuth: true },
      },
      {
        name: "NOTIFICATIONS_SETTINGS",
        path: "/settings/notifications",
        component: () => import(`pages/Settings/Notifications`),
        meta: { requireAuth: true },
      },
      {
        name: "AGENT_SALES",
        path: "/agent-sales",
        component: () =>
          import(/* webpackChunkName: "agent-sales" */ "pages/AgentSales/index.vue"),
        meta: { requireAuth: true },
      },
      {
        name: "NEW_AGENT_SALE_REQUEST",
        component: () =>
          import(
            /* webpackChunkName: "agent-sales" */ "pages/AgentSales/NewRequest/index.vue"
          ),
        meta: { requireAuth: true },
        path: "/agent-sales/new",
      },

      {
        name: "AGENT_SALES_PAYMETNS",
        path: "/agent-sales/payments",
        component: () =>
          import(
            /* webpackChunkName: "agent-sales" */ "pages/AgentSales/Payments/index.vue"
          ),
        meta: { requireAuth: true },
      },

      {
        name: "AGENT_SALES_PRODUCTS",
        path: "/agent-sales/products",
        component: () =>
          import(
            /* webpackChunkName: "agent-sales" */ "pages/AgentSales/Products/index.vue"
          ),
        meta: { requireAuth: true },
      },

      {
        name: "AGENT_SALES_ORDERS",
        path: "/agent-sales/orders",
        component: () =>
          import(
            /* webpackChunkName: "agent-sales" */ "pages/AgentSales/Orders/index.vue"
          ),
        meta: { requireAuth: true },
      },

      {
        name: "AGENT_SALE_REQUEST",
        component: () =>
          import(
            /* webpackChunkName: "agent-sales" */ "pages/AgentSales/NewRequest/index.vue"
          ),
        meta: { requireAuth: true },
        path: "/agent-sales/request/:id",
      },
      {
        name: "LAW_HELP",
        component: () => import("pages/LawHelpRequest/index.vue"),
        path: "/law-help-request",
        meta: { requireAuth: true },
      },
      {
        name: "SEND_MESSAGE",
        component: () => import("pages/SendMessage/index.vue"),
        path: "/send-message",
        meta: { requireAuth: true },
      },
      {
        name: "SERVICES",
        component: () =>
          import(/* webpackChunkName: "services" */ "pages/Services/index.vue"),
        path: "/services",
        meta: { requireAuth: true, section: "SERVICES" },
      },
      {
        name: "SERVICES_LIST",
        component: () =>
          import(/* webpackChunkName: "services" */ "pages/Services/services.vue"),
        path: "/services/all",
        meta: { requireAuth: true, section: "SERVICES" },
      },
      {
        name: "ADD_SERVICE",
        component: () =>
          import(/* webpackChunkName: "services" */ "pages/Services/add.vue"),
        path: "/services/order/:id",
        meta: { requireAuth: true, section: "SERVICES", hide_banners: true },
      },
      {
        name: "VIEW_SERVICE",
        component: () =>
          import(/* webpackChunkName: "services" */ "pages/Services/service"),
        path: "/services/info/:id",
        meta: { requireAuth: true, section: "SERVICES" },
      },
      {
        name: "IMPORT_PRODUCTS",
        component: () => import("pages/ImportProducts/index.vue"),
        path: "/products/import",
        meta: { requireAuth: true },
      },
      {
        name: "CALL",
        component: () => import("pages/Call"),
        path: "/call",
        meta: { requireAuth: true },
      },
      {
        name: "WITHDRAWAL_AGREEMENTS",
        component: () => import("pages/WithdrawalAgreements"),
        path: "/withdrawal-agreements",
        meta: { requireAuth: true },
      },
    ],
  },
  {
    name: "APP_LOGOUT",
    path: "/logout",
    component: () => import("pages/Logout"),
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    name: "404",

    component: () => import("pages/Error404.vue"),
  },
];

export default routes;

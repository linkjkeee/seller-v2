import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function({ store, router } /* { store, ssrContext } */) {
  // console.log("Router store scope:", store);

  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0, behavior: "smooth" }),
    routes,
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeResolve(async (to, from, next) => {
    // Router.beforeEach(async (to, from, next) => {
    let known_user = store.getters["auth/is_logged"];
    let is_auth_page = ["AUTH_LOGIN", "AUTH_REGISTER"].includes(to.name);
    let need_auth = !!to.matched.some((record) => record.meta.requireAuth);

    if (is_auth_page) {
      if (known_user) {
        next("/");
      } else {
        next();
      }
    }
    // ? NOT AUTH PAGES:
    else {
      if (need_auth) {
        if (known_user) {
          next();
        } else {
          next({ path: "/login", query: { cameFrom: to.fullPath } });
        }
      } else {
        next();
      }
    }
  });

  return Router;
}
